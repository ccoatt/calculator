﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationModel
{
    public class LoanRentModel
    {
        public Int64 Cash { get; set; }
        public double CashDown { get; set; }
        public int TimePerMonth { get; set; }
        public double RateRent { get; set; }
        public double RateRentTrue { get; set; }

        public string CashStr { get; set; }
        public string CashDownStr { get; set; }
        public string TimePerMonthStr { get; set; }
        public string RateRentStr { get; set; }
        public string RateRentTrueStr  { get; set; }
    }
}
