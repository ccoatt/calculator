﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationModel
{
    public class OutputModel
    {
        public double ShowCashResult { get; set; }
        public double ShowCashTotal { get; set; }
        public double ShowInterestTotal { get; set; }
        public double ShowTotalCashWithInterest { get; set; }
        public double ShowCashRentPerMonth { get; set; }
        public double ShowCashRentDiscountPerMonth { get; set; }
        public double ShowDiscountPerMonth { get; set; }
        public double ShowVat { get; set; }
        public double ShowVatPerMonth { get; set; }
        public double ShowPaymentPerMonth { get; set; }
//        public int ShowTime { get; set; }

        public int ShowAge { get; set; }
        public string ShowGender { get; set; }
        public double ShowRateInsurance { get; set; }
        public double ShowPriceInsurance { get; set; }
    }
}
