﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationModel
{
    public class InsuranceModel
    {
        public string InsuranceCompany { get; set; }
        public int Birthday { get; set; }
        public int BirthdayYear { get; set; }
        public int BirthdayMonth { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public double RateInsurance { get; set; }

    }
}
