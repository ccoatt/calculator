﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using CalculateService;
using Calculator;


namespace TestCalculator
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class TestCalculator
    {
        public TestCalculator()
        {
        }

        [TestMethod]
        public void CodedUITestMethod1()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //    // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //    // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        
        [TestMethod]
        public void TestCalCashRasult()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysLoan(100000, 30000, 24, 15, 12);

            double expectCalCashResult = calT.m_calCashResult;
            Assert.AreEqual(70000, expectCalCashResult);
        }

        [TestMethod]
        public void TestCalInterestTotal()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysLoan(100000, 30000, 24, 15, 12);

            double expectCalInterestTotal = calT.m_calInterestTotal;
            Assert.AreEqual(21000, expectCalInterestTotal);
        }

        [TestMethod]
        public void TestCalTotalCashWithInterest()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysLoan(100000, 30000, 24, 15, 12);
            double expectCalTotalCashWithInterest = calT.m_calTotalCashWithInterest;
            Assert.AreEqual(91000, expectCalTotalCashWithInterest);
        }

        [TestMethod]
        public void TestCalCashRentPerMonth()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysLoan(100000, 30000, 24, 15, 12);
            double expectCalCashRentPerMonth = calT.m_calCashRentPerMonth;
            Assert.AreEqual(3791.67, expectCalCashRentPerMonth);
        }

        [TestMethod]
        public void TestCalCashRentDiscountPerMonth()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysLoan(100000, 30000, 24, 15, 12);
            double expectCalCashRentDiscountPerMonth = calT.calCashRentDiscountPerMonth;
            Assert.AreEqual(3616.67, expectCalCashRentDiscountPerMonth);
        }

        [TestMethod]
        public void TestCalDiscountPerMonth()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysLoan(100000, 30000, 24, 15, 12);
            double expectCalDiscountPerMonth = calT.calDiscountPerMonth;
            Assert.AreEqual(175, expectCalDiscountPerMonth);
        }

        [TestMethod]
        public void TestCalRentCashResult()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysRent(250000, 25000, 36, 15, 12);
            double expectCalCashResult = calT.m_calCashResult;
            Assert.AreEqual(225000, expectCalCashResult);
        }

        [TestMethod]
        public void TestCalRentInterestTotal()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysRent(250000, 25000, 36, 15, 12);
            double expectCalInterestTotal = calT.m_calInterestTotal;
            Assert.AreEqual(101250, expectCalInterestTotal);
        }

        [TestMethod]
        public void TestCalRentVat()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysRent(250000, 25000, 36, 15, 12);
            double expectCalVat = calT.calVat;
            Assert.AreEqual(22837.50, expectCalVat);
        }

        [TestMethod]
        public void TestCalRentTotalCashWithInterest()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysRent(250000, 25000, 36, 15, 12);
            double expectCalTotalCashWithInterest = calT.m_calTotalCashWithInterest;
            Assert.AreEqual(349087.50, expectCalTotalCashWithInterest);
        }

        [TestMethod]
        public void TestCalRentPaymentPerMonth()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysRent(250000, 25000, 36, 15, 12);
            double expectCalPaymentPerMonth = calT.calPaymentPerMonth;
            Assert.AreEqual(9062.50, expectCalPaymentPerMonth);
        }

        [TestMethod]
        public void TestCalRentVatPerMonth()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysRent(250000, 25000, 36, 15, 12);
            double expectCalVatPerMonth = calT.calVatPerMonth;
            Assert.AreEqual(634.38, expectCalVatPerMonth);
        }

        [TestMethod]
        public void TestCalRentCashRentPerMonth()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysRent(250000, 25000, 36, 15, 12);
            double expectCalCashRentPerMonth = calT.m_calCashRentPerMonth;
            Assert.AreEqual(9696.88, expectCalCashRentPerMonth);
        }

        [TestMethod]
        public void TestCalRentDiscoutPerMonth()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysRent(250000, 25000, 36, 15, 12);
            double expectCalDiscountPerMonth = calT.calDiscountPerMonth;
            Assert.AreEqual(601.88, expectCalDiscountPerMonth);
        }

        [TestMethod]
        public void TestCalRentCashDiscoutPerMonth()
        {
            CalculatorForm calT = new CalculatorForm();
            calT.SysRent(250000, 25000, 36, 15, 12);
            double expectCalCashRentDiscountPerMonth = calT.calCashRentDiscountPerMonth;
            Assert.AreEqual(9095.00, expectCalCashRentDiscountPerMonth);
        }

    }


}
