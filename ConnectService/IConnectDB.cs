﻿using System;
namespace ConnectService
{
    public interface IConnectDB
    {
        double Connect(string company, string gender, int age, int time);
    }
}
