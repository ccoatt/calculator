﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Data;
//using CalculationModel;

namespace ConnectService
{
    public class ConnectDB : IConnectDB
    {
        public double Connect(string company ,string gender,int age,int time)
        {
            string strFile = @"D:\Program Files\Microsoft Visual Studio 11.0\Projects\Calculator\DB\Prakun_AIA_MTL.xlsx";
            string connectionString = "";
            if (strFile.Trim().EndsWith(".xlsx"))
                connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES\";", strFile);
            else if (strFile.Trim().EndsWith(".xls"))
            {
                connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes\";", strFile);
            }

            DataSet excelDataSet = new DataSet();
            DataTable Contents = new DataTable();

            // คัวแแปร
            time = time / 12;
            switch (gender)
            {
                case "ชาย": gender = "MALE";
                    break;
                case "หญิง": gender = "FEMALE";
                    break;
            }
            
            string sql = "";
            switch (company)
            {
                    
                case "AIA": //company = "Prakun_AIA";
                    sql = "SELECT " + gender + "  FROM [Prakun_AIA$] WHERE AGE_1 <= " + age.ToString() + " AND AGE_2 >= " + age.ToString() + "";
                    break;
                case "MTL": //company = "Prakun_MTL";
                    sql = "SELECT " + gender + "  FROM [Prakun_MTL$] WHERE AGE_1 <= " + age.ToString() + " AND AGE_2 >= " + age.ToString() + " AND PK_YEAR = " + time.ToString() + "";
                    break;
            }

            using (OleDbConnection con = new System.Data.OleDb.OleDbConnection(connectionString))
            {
                con.Open();
                OleDbDataAdapter cmd = new OleDbDataAdapter(sql, con);
                cmd.Fill(excelDataSet);
                cmd.Fill(Contents);
                
                con.Close();
                if (Contents.Rows.Count == 0)
                {
                    return 0.00;
                }
                else
                {
                    return (double)Contents.Rows[0][0];
                }
            }
        }
    }
}
