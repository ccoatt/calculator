﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CalculateService;

namespace Calculator
{
    public partial class CalculatorForm : Form
    {
        public CalculatorForm()
        {
            InitializeComponent();
            rdoNoInsurance.CheckedChanged += new EventHandler(radioTypeInsurance_CheckedChanged);
            rdoAIA.CheckedChanged += new EventHandler(radioTypeInsurance_CheckedChanged);
            rdoMTL.CheckedChanged += new EventHandler(radioTypeInsurance_CheckedChanged);

            rdoLoan.CheckedChanged += new EventHandler(radioTypeAgreement_CheckedChanged);
            rdoRent.CheckedChanged += new EventHandler(radioTypeAgreement_CheckedChanged);
        }

        double cash { get; set; }
        double cashDown { get; set; }
        double timePerMonth { get; set; }
        double rateRent { get; set; }
        double rateRentTrue { get; set; } 

        public double calCashResult { get; set; }
        public double calInterestTotal { get; set; }
        public double calTotalCashWithInterest { get; set; }
        public double calCashRentPerMonth { get; set; }
        public double calCashRentDiscountPerMonth { get; set; }
        public double calDiscountPerMonth { get; set; }
        public double calVat { get; set; }
        public double calVatPerMonth { get; set; }
        public double calPaymentPerMonth { get; set; }


        Calculate cal = new Calculate();

        private void CalculatorForm_Load(object sender, EventArgs e)
        {

        }

        /// ปุ่มคำนวน
        private void btnCalculator_Click(object sender, EventArgs e)
        {

            string checkAgree = CheckAgreement();
            string checkIns = CheckInsurance();

            dtpBirthday.MaxDate = DateTime.Now;

            /// เช็คค่าว่าง
            bool checkSpaceEmpty = CheckSpaceEmpty();
            if (checkSpaceEmpty == true)
            {

                switch (checkAgree)
                {
                    case "Loan":
                        SysLoan(cash, cashDown, timePerMonth, rateRent, rateRentTrue);
                        break;
                    case "Rent":
                        SysRent(cash, cashDown, timePerMonth, rateRent, rateRentTrue);
                        break;

                }

                switch (checkIns)
                {
                    case "NoInsurance":
                        break;
                    case "AIA":
                        break;
                    case "MTL":
                        break;
                }

            }

        } 
        
        // ประเภท
        public string  CheckAgreement()
        {
            if (rdoLoan.Checked == true)
            {
                return "Loan";
            }
            else if (rdoRent.Checked == true)
            {
                return "Rent";
            }
            else
            {
                return "False";
            }
        }

        // เช็คค่าว่าง
        private bool  CheckSpaceEmpty()
        {
            string cashStr = txtCash.Text;
            string cashDownStr = txtCashDown.Text;
            string timePerMonthStr = txtTimeRentPerMonth.Text;
            string rateRentStr = txtRateInterestRent.Text;
            string rateRentTrueStr = txtRateInterestRentTrue.Text;

            if (cashStr == "" || cashDownStr == "" || timePerMonthStr == "" || rateRentStr == "" || rateRentTrueStr == "")
            {
                lbBaht.Text += "*";
                lbBaht2.Text += "*";
                lbMonth.Text += "*";
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ");
                return false;
            }else{
                 cash = double.Parse(cashStr);
                 cashDown = double.Parse(cashDownStr);
                 timePerMonth = double.Parse(timePerMonthStr);
                 rateRent = double.Parse(rateRentStr);
                 rateRentTrue = double.Parse(rateRentTrueStr);

                 if (cash >= cashDown)
                 {
                     return true;
                 }
                 else
                 {
                     MessageBox.Show("กรุณากรอกค่าเงินดาวน์ใหม่");
                     return false;
                 }
            }
        }

        // เงินกู้
        public void SysLoan(double cash, double cashDown, double timePerMonth, double rateRent, double rateRentTrue)
        {
            // คำนวน ราคาส่วนที่เหลือ 
            calCashResult = cal.CalCashRasult(cash, cashDown);
            txtCashResult.Text = calCashResult.ToString("#,###.00");
            txtCashRentTotal.Text = calCashResult.ToString("#,###.00");

            // คำนวนดอกเบี้ยเช่าทั้งหมด
            calInterestTotal = cal.CalInterestTotal(calCashResult, rateRent, timePerMonth);
            txtInterestTotal.Text = calInterestTotal.ToString("#,###.00");

            // ค่าเชื่อซื้อรวมดอกเบี้ย(ทุนประกัน)
            calTotalCashWithInterest = cal.CalTotalCashWithInterest(calCashResult, calInterestTotal);
            txtTotalCashWithInterest.Text = calTotalCashWithInterest.ToString("#,###.00");

            // ค่าเช่าซื้อ/เดือน
            calCashRentPerMonth = cal.CalCashRentPerMonth(calTotalCashWithInterest, timePerMonth);
            txtCashRentPerMonth.Text = calCashRentPerMonth.ToString("#,###.00");

            // ค่าเช่าซื้อส่วนลด/เดือน
            calCashRentDiscountPerMonth = cal.CalCashRentDiscountPerMonth(calCashResult, rateRentTrue, timePerMonth);
            txtCashRentDiscountPerMonth.Text = calCashRentDiscountPerMonth.ToString("#,###.00");

            // ส่วนลด/เดือน
            calDiscountPerMonth = cal.CalDiscountPerMonth(calCashRentDiscountPerMonth, calCashRentPerMonth);
            txtDiscountPerMonth.Text = calDiscountPerMonth.ToString("#,###.00");
        }

        // เช่าซื้อรถ
        public void SysRent(double cash, double cashDown, double timePerMonth, double rateRent, double rateRentTrue)
        {
            // คำนวน ราคาส่วนที่เหลือ 
            calCashResult = cal.CalCashRasult(cash, cashDown);
            txtCashResult.Text = calCashResult.ToString("#,###.00");
            txtCashRentTotal.Text = calCashResult.ToString("#,###.00");

            // คำนวนดอกเบี้ยเช่าทั้งหมด
            calInterestTotal = cal.CalInterestTotal(calCashResult, rateRent, timePerMonth);
            txtInterestTotal.Text = calInterestTotal.ToString("#,###.00");

            // คำนวน vat
            calVat = cal.CalVat7(calCashResult, calInterestTotal);
            txtVat.Text = calVat.ToString("#,###.00");

            // ค่าเชื่อซื้อรวมดอกเบี้ย(ทุนประกัน)
            calTotalCashWithInterest = cal.CalTotalCashWithInterest(calCashResult, calInterestTotal ,calVat);
            txtTotalCashWithInterest.Text = calTotalCashWithInterest.ToString("#,###.00");

            // ค่างวด/เดือน
            calPaymentPerMonth = cal.CalPaymentPerMonth(calCashResult, calInterestTotal, timePerMonth);
            txtPaymentPerMonth.Text = calPaymentPerMonth.ToString("#,###.00");

            // ค่าภาษี / เดือน
            calVatPerMonth = cal.CalVatPerMonth(calVat, timePerMonth);
            txtVatPerMonth.Text = calVatPerMonth.ToString("#,###.00");

            // ค่าเช่าซื้อ/เดือน
            calCashRentPerMonth = cal.CalCashRentPerMonth(calTotalCashWithInterest, timePerMonth);
            txtCashRentPerMonth.Text = calCashRentPerMonth.ToString("#,###.00");

            // ค่าเช่าซื้อส่วนลด/เดือน
            calCashRentDiscountPerMonth = cal.CalCashRentDiscountPerMonthVat(calCashResult, rateRentTrue, timePerMonth);
            txtCashRentDiscountPerMonth.Text = calCashRentDiscountPerMonth.ToString("#,###.00");

            // ส่วนลด/เดือน
            calDiscountPerMonth = cal.CalDiscountPerMonth(calCashRentDiscountPerMonth, calCashRentPerMonth);
            txtDiscountPerMonth.Text = calDiscountPerMonth.ToString("#,###.00");
        }

        /// ตรวจสอบตัวอัการ
        int cnt;
        public void DontKeyChar(object sender, KeyPressEventArgs e)
        {
            
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.') )
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
            if ((sender as TextBox).Text.EndsWith("."))
            {
                cnt = (sender as TextBox).Text.Length;
            }

            // ทศนิยม 2 ตำแหน่ง
            if ((sender as TextBox).SelectionStart >= cnt + 2 && ((sender as TextBox).Text.IndexOf('.') > -1) && e.KeyChar != 8)
            {
                e.Handled = true;
                //MessageBox.Show((sender as TextBox).SelectionStart.ToString() + Environment.NewLine + cnt);
            }

            if ((sender as TextBox).SelectionStart < cnt && e.KeyChar == 8)
            {
                cnt= (sender as TextBox).Text.IndexOf('.');
                MessageBox.Show(cnt.ToString());
            }
            if ((sender as TextBox).SelectionStart < cnt && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                cnt++;
                //MessageBox.Show(cnt.ToString());
            }


        }



        public void txtRateInterestRent_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyChar(sender, e);     
        }

        private void txtRateInterestRentTrue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyChar(sender, e);
        }

        private void txtCash_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyChar(sender, e);        
        }

        private void txtCashDown_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyChar(sender, e);
        }

        private void txtTimeRentPerMonth_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyChar(sender, e);
        }

        public void FormatNumber(object sender, EventArgs e)
        {
            if ((sender as TextBox).Text != "")
            {
                (sender as TextBox).Text = string.Format("{0:#,###.##}", double.Parse((sender as TextBox).Text));
            }
        }

        private void txtCash_Leave(object sender, EventArgs e)
        {
            FormatNumber(sender, e);
        }

        private void txtCashDown_Leave(object sender, EventArgs e)
        {
            FormatNumber(sender, e);
        }

        private void txtTimeRentPerMonth_Leave(object sender, EventArgs e)
        {
            FormatNumber(sender, e);
        }

        private void txtRateInterestRent_Leave(object sender, EventArgs e)
        {
            FormatNumber(sender, e);
        }

        private void txtRateInterestRentTrue_Leave(object sender, EventArgs e)
        {
            FormatNumber(sender, e);
        }

        private void ResetData()
        {
            txtInterestTotal.Text = null;
            txtVat.Text = null;
            txtTotalCashWithInterest.Text = null;
            txtPaymentPerMonth.Text = null;
            txtVatPerMonth.Text = null;
            txtCashRentPerMonth.Text = null;
            txtDiscountPerMonth.Text = null;
            txtCashRentDiscountPerMonth.Text = null;
            lbBaht.Text = "บาท";
            lbBaht2.Text = "บาท";
            lbMonth.Text = "เดือน";
        }

        // คืนค่าาา
        private void btnReset_Click(object sender, EventArgs e)
        {
            txtCash.Text = null;
            txtCashDown.Text = null;
            txtCashResult.Text = null;
            txtCashRentPerMonth.Text = null;
            txtCashRentTotal.Text = null;
            txtTimeRentPerMonth.Text = null;
            txtRateInterestRent.Text = "15";
            txtRateInterestRentTrue.Text = "12";

            ResetData();
        }

        // คืนค่าเมื่อเปลี่ยนประเภทสัญญา
        private void radioTypeAgreement_CheckedChanged(object sender, EventArgs e)
        {
            ResetData();
        }

        // เช็คประกัน
        public string CheckInsurance()
        {


            if (rdoNoInsurance.Checked == true)
            {
                return "NoInsurance";
            }
            else if (rdoAIA.Checked == true)
            {
                return "AIA";
            }
            else if (rdoMTL.Checked == true)
            {
                return "MTL";
            }
            else
            {
                return "Error";
            }
        }


        // เปลี่ยนค่าประกัน 
        private void radioTypeInsurance_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;
            if (rdoNoInsurance.Checked)
            {
                txtCompanyInsurance.Text = "ไม่ทำประกัน";
            }
            else if (rdoAIA.Checked)
            {
                txtCompanyInsurance.Text = "AIA";
            }
            else if (rdoMTL.Checked)
            {
                txtCompanyInsurance.Text = "MTL";
            }
            ResetData();
        }

        private void dtpBirthday_ValueChanged(object sender, EventArgs e)
        {
            //int age = DateTime.Today.Year - dtpBirthday.Value.Year;// CurrentYear - BirthDate
            int birthday = dtpBirthday.Value.Year;
            numAge.Value = cal.CalAge(birthday); 
        }

        private void txtRateInterestInsurance_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyChar(sender,e);
        }

        private void cboSex_SelectedValueChanged(object sender, EventArgs e)
        {
            ResetData();
        }


    }
}
