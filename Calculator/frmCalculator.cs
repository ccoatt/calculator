﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CalculateService;
using CalculationModel;
using System.Data.OleDb;


namespace Calculator
{
    public partial class frmCalculator : Form
    {
        CalculationLoan m_calL = new CalculationLoan();
        CalculationInsurance m_calIns = new CalculationInsurance();
        LoanRentModel m_loanRentModel = new LoanRentModel();
        OutputModel m_outputModel = new OutputModel();
        InsuranceModel m_InsModel = new InsuranceModel();



        public frmCalculator()
        {
            InitializeComponent();
            rdoNoInsurance.CheckedChanged += new EventHandler(radioTypeInsurance_CheckedChanged);
            rdoAIA.CheckedChanged += new EventHandler(radioTypeInsurance_CheckedChanged);
            rdoMTL.CheckedChanged += new EventHandler(radioTypeInsurance_CheckedChanged);

            rdoLoan.CheckedChanged += new EventHandler(radioTypeAgreement_CheckedChanged);
            rdoRent.CheckedChanged += new EventHandler(radioTypeAgreement_CheckedChanged);
        }


        private void CalculatorForm_Load(object sender, EventArgs e)
        {
            ResetData(); 
            dtpBirthday.MaxDate = DateTime.Now;
            dtpBirthday.Enabled = false;
            cboGender.Enabled = false;
        }

        public void CheckDataExcel()
        {
            if (m_outputModel.ShowRateInsurance == 0.00)
            {
                MessageBox.Show("ไม่พบข้อมูลในฐานข้อมูล","Status",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        /// ปุ่มคำนวน
        private void btnCalculator_Click(object sender, EventArgs e)
        {       
            ResetData();
            GetVariableText();
            bool ischeckSpaceEmpty = CheckSpaceEmpty();
            if (ischeckSpaceEmpty == true)
            {
                SetVariable();
                bool isCashDiff = CheckCashDiff();
                if (rdoLoan.Checked && rdoNoInsurance.Checked && isCashDiff == true)
                {
                    m_outputModel = m_calL.CalSysLoan(m_loanRentModel);
                    GetShowVariable();
                }
                else if (rdoLoan.Checked && rdoAIA.Checked && isCashDiff == true)
                {
                    m_outputModel = m_calL.CalSysLoan(m_loanRentModel);
                    m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
                    m_outputModel = m_calL.CalSysLoan(m_loanRentModel, m_outputModel);
                    GetShowVariable();
                    GetShowVariableIns();          
                    
                }
                else if (rdoLoan.Checked && rdoMTL.Checked && isCashDiff == true)
                {
                    m_outputModel = m_calL.CalSysLoan(m_loanRentModel);
                    m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
                    m_outputModel = m_calL.CalSysLoan(m_loanRentModel, m_outputModel);
                    GetShowVariable();
                    GetShowVariableIns();
                }
                    /////////////////// เช่าซื้อ
                else if (rdoRent.Checked && rdoNoInsurance.Checked && isCashDiff == true)
                {
                    m_outputModel = m_calL.CalSysRent(m_loanRentModel);
                    //m_Cr.CalSysRent(m_loanRentModel);
                    GetShowVariable();
                }
                else if (rdoRent.Checked && rdoAIA.Checked && isCashDiff == true)
                {
                    m_outputModel = m_calL.CalSysRent(m_loanRentModel);
                    m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
                    m_outputModel = m_calL.CalSysRent(m_loanRentModel, m_outputModel);
                    GetShowVariable();
                    GetShowVariableIns();
                }
                else if (rdoRent.Checked && rdoMTL.Checked && isCashDiff == true)
                {
                    m_outputModel = m_calL.CalSysRent(m_loanRentModel);
                    m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
                    m_outputModel = m_calL.CalSysRent(m_loanRentModel, m_outputModel);
                    GetShowVariable();
                    GetShowVariableIns();
                }
            }
        }

        private void GetShowVariable()
        {
            txtCashResult.Text = m_outputModel.ShowCashResult.ToString("#,##0.00");
            txtCashRentTotal.Text = m_outputModel.ShowCashTotal.ToString("#,##0.00");
            txtInterestTotal.Text = m_outputModel.ShowInterestTotal.ToString("#,##0.00");
            txtTotalCashWithInterest.Text = m_outputModel.ShowTotalCashWithInterest.ToString("#,##0.00");
            txtCashRentPerMonth.Text = m_outputModel.ShowCashRentPerMonth.ToString("#,##0.00");
            txtCashRentDiscountPerMonth.Text = m_outputModel.ShowCashRentDiscountPerMonth.ToString("#,##0.00");
            txtDiscountPerMonth.Text = m_outputModel.ShowDiscountPerMonth.ToString("#,##0.00");

            txtVat.Text = m_outputModel.ShowVat.ToString("#,##0.00");
            txtPaymentPerMonth.Text = m_outputModel.ShowPaymentPerMonth.ToString("#,##0.00");
            txtVatPerMonth.Text = m_outputModel.ShowVatPerMonth.ToString("#,##0.00");
        }

        private void GetShowVariableIns()
        {
            txtPriceInsurance.Text = m_outputModel.ShowPriceInsurance.ToString("#,##0.00");
            txtRateInterestInsurance.Text = m_outputModel.ShowRateInsurance.ToString("#,##0.000");
            
        }

        private void GetVariableText()
        {
            m_loanRentModel.CashStr = txtCash.Text;
            m_loanRentModel.CashDownStr = txtCashDown.Text;
            m_loanRentModel.TimePerMonthStr = txtTimeRentPerMonth.Text;
            m_loanRentModel.RateRentStr = txtRateInterestRent.Text;
            m_loanRentModel.RateRentTrueStr = txtRateInterestRentTrue.Text;
        }

        private void SetVariable()
        {
            m_loanRentModel.Cash = Int64.Parse(ReplaceFormatNumber());
            m_loanRentModel.CashDown = double.Parse(m_loanRentModel.CashDownStr);
            m_loanRentModel.TimePerMonth = int.Parse(m_loanRentModel.TimePerMonthStr);
            m_loanRentModel.RateRent = double.Parse(m_loanRentModel.RateRentStr);
            m_loanRentModel.RateRentTrue = double.Parse(m_loanRentModel.RateRentTrueStr);

            m_InsModel.InsuranceCompany = txtCompanyInsurance.Text;
            m_InsModel.Gender = cboGender.Text;
            m_InsModel.BirthdayYear = dtpBirthday.Value.Year;
            m_InsModel.BirthdayMonth = dtpBirthday.Value.Month;
            m_InsModel.Birthday = dtpBirthday.Value.Day;
        }

        // เช็คค่าว่าง
        private bool CheckSpaceEmpty()
        {
            if (m_loanRentModel.RateRentStr == ""  || m_loanRentModel.RateRentTrueStr == "")
            {
                MessageBox.Show("กรุณากรอกอักตราดอกเบี้ย", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (m_loanRentModel.CashStr == "")
            {
                MessageBox.Show("กรุณากรอกเงินสด", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (m_loanRentModel.CashDownStr == "")
            {
                MessageBox.Show("กรุณากรอกเงิดาวน์", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            if (m_loanRentModel.TimePerMonthStr == "")
            {
                MessageBox.Show("กรุณากรอกระยะเวลา", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private bool CheckCashDiff()
        {
            if (m_loanRentModel.Cash >= m_loanRentModel.CashDown)
            {
                return true;
            }
            else
            {
                MessageBox.Show("กรุณากรอกค่าเงินดาวน์ใหม่", "แจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void dtpBirthday_ValueChanged(object sender, EventArgs e)
        {
            GetVariableText();
            bool ischeckSpaceEmpty = CheckSpaceEmpty();
            if (ischeckSpaceEmpty == true)
            {
                ResetData();
                SetVariable();
                m_outputModel = m_calIns.CalAgeRate(m_InsModel,m_loanRentModel, m_outputModel);
                txtAge.Text = m_outputModel.ShowAge.ToString();
                txtRateInterestInsurance.Text = m_outputModel.ShowRateInsurance.ToString("#,##0.000");
                CheckDataExcel();
            }
        }

        private void cboSex_SelectedValueChanged(object sender, EventArgs e)
        {
            GetVariableText();
            bool ischeckSpaceEmpty = CheckSpaceEmpty();
            if (ischeckSpaceEmpty == true)
            {
                ResetData();
                SetVariable();
                m_outputModel = m_calIns.CalAgeRate(m_InsModel, m_loanRentModel, m_outputModel);
                txtAge.Text = m_outputModel.ShowAge.ToString();
                txtRateInterestInsurance.Text = m_outputModel.ShowRateInsurance.ToString("#,##0.000");
                CheckDataExcel();
            }
        }

        /// ตรวจสอบตัวอักษร
        public void DontKeyCharDouble(object sender, KeyPressEventArgs e)
        {
            
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.') )
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        public void DontKeyCharInt(object sender, KeyPressEventArgs e)
        {

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) ||
                (e.KeyChar == '.'))
            {
                e.Handled = true;
            }

            if((sender as TextBox).SelectionStart >= 13 && e.KeyChar != 8 ){
                e.Handled = true;
            }

            if ((sender as TextBox).SelectionStart > (sender as TextBox).Text.IndexOf('.') && (sender as TextBox).Text.IndexOf('.') > -1 )
            {
                e.Handled = true;
            }
        }

        private void txtCash_KeyUp(object sender, KeyEventArgs e)
        {
            autoCal();
        }

        private void txtCashDown_KeyUp(object sender, KeyEventArgs e)
        {
            autoCal();
        }

        public void autoCal()
        {
            if (txtCash.Text != "" && txtCashDown.Text != "" && txtCash.Text.Length > 3 && txtCash.Text.IndexOf('.') > -1)
            {
                Int64 cash = Int64.Parse(ReplaceFormatNumber());
                double cashDown = double.Parse(txtCashDown.Text);
                m_outputModel.ShowCashResult = m_calL.CalCashRasult(cash, cashDown);
                txtCashResult.Text = m_outputModel.ShowCashResult.ToString("#,##0.00");
                txtCashRentTotal.Text = m_outputModel.ShowCashTotal.ToString("#,##0.00");
            }
            else if (txtCash.Text != "" && txtCashDown.Text != "" && txtCash.Text.IndexOf('.') == -1)
            {
                Int64 cash = Int64.Parse(ReplaceFormatNumber2());
                double cashDown = double.Parse(txtCashDown.Text);
                m_outputModel.ShowCashResult = m_calL.CalCashRasult(cash, cashDown);
                txtCashResult.Text = m_outputModel.ShowCashResult.ToString("#,##0.00");
                txtCashRentTotal.Text = m_outputModel.ShowCashTotal.ToString("#,##0.00");
            }
        }
 
        private string ReplaceFormatNumber()
        {
            string text = (txtCash.Text).Replace(",", "");
            return text.Substring(0, (text.Length - 3));
        }

        private string ReplaceFormatNumber2()
        {
            string text = (txtCash.Text).Replace(",", "");
            return text;
        }

        public void txtRateInterestRent_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyCharDouble(sender, e);     
        }

        private void txtRateInterestRentTrue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyCharDouble(sender, e);
        }

        private void txtCash_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyCharInt(sender, e);        
        }

        private void txtCashDown_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyCharDouble(sender, e);
        }

        private void txtTimeRentPerMonth_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyCharInt(sender, e);
        }

        public void FormatNumberInt(object sender, EventArgs e)
        {

            if ((sender as TextBox).Text != "")
            {
                (sender as TextBox).Text = string.Format("{0:n}", double.Parse((sender as TextBox).Text));
                (sender as TextBox).SelectionStart = (sender as TextBox).Text.Length;
            }
        }

        public void FormatNumberDouble(object sender, EventArgs e)
        {

            if ((sender as TextBox).Text != "")
            {
                (sender as TextBox).Text = string.Format("{0:n}", double.Parse((sender as TextBox).Text));
                (sender as TextBox).SelectionStart = (sender as TextBox).Text.Length;
            }
        }

        private void txtCash_Leave(object sender, EventArgs e)
        {
            FormatNumberInt(sender, e);
        }

        private void txtCashDown_Leave(object sender, EventArgs e)
        {
            FormatNumberDouble(sender, e);
        }

        private void ResetData()
        {
            txtInterestTotal.Text = null;
            txtVat.Text = null;
            txtTotalCashWithInterest.Text = null;
            txtPaymentPerMonth.Text = null;
            txtVatPerMonth.Text = null;
            txtCashRentPerMonth.Text = null;
            txtDiscountPerMonth.Text = null;
            txtCashRentDiscountPerMonth.Text = null;
            lbBaht.Text = "บาท";
            lbBaht2.Text = "บาท";
            lbMonth.Text = "เดือน";
            txtCashRentPerMonth.Text = null;
            
            txtPriceInsurance.Text = "0.00";
            //txtRateInterestInsurance.Text = "0.00";
            //txtAge.Text = "0";
            //dtpBirthday.Value= DateTime.Now; 

        }

        // คืนค่าาา
        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetData();

            txtCash.Text = "";
            txtCashDown.Text = "";
            txtRateInterestRent.Text = "15";
            txtRateInterestRentTrue.Text = "12";
            txtTimeRentPerMonth.Text = "";
            txtCashResult.Text = "";
            txtCashRentTotal.Text = "";
            txtRateInterestInsurance.Text = "0.00";
            txtAge.Text = "0";
            txtRateInterestInsurance.Text = "0.00";
        }

        // คืนค่าเมื่อเปลี่ยนประเภทสัญญา
        private void radioTypeAgreement_CheckedChanged(object sender, EventArgs e)
        {
            ResetData();
        }

        // เปลี่ยนค่าประกัน 
        private void radioTypeInsurance_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;
            if (rdoNoInsurance.Checked)
            {
                txtCompanyInsurance.Text = "ไม่ทำประกัน";
                dtpBirthday.Enabled = false;
                cboGender.Enabled = false;
            }
            else if (rdoAIA.Checked)
            {
                txtCompanyInsurance.Text = "AIA";
                dtpBirthday.Enabled = true;
                cboGender.Enabled = true;
            }
            else if (rdoMTL.Checked)
            {
                txtCompanyInsurance.Text = "MTL";
                dtpBirthday.Enabled = true;
                cboGender.Enabled = true;
            }
            ResetData();
        }

        private void txtRateInterestInsurance_KeyPress(object sender, KeyPressEventArgs e)
        {
            DontKeyCharDouble(sender,e);
        }

        private void rdoAIA_Click(object sender, EventArgs e)
        {
            GetVariableText();
            bool ischeckSpaceEmpty = CheckSpaceEmpty();
            if (ischeckSpaceEmpty == true)
            {
                SetVariable();
                m_outputModel = m_calIns.CalAgeRate(m_InsModel, m_loanRentModel, m_outputModel);
                txtAge.Text = m_outputModel.ShowAge.ToString();
                txtRateInterestInsurance.Text = m_outputModel.ShowRateInsurance.ToString();
            }
        }

        private void rdoMTL_Click(object sender, EventArgs e)
        {
            GetVariableText();
            bool ischeckSpaceEmpty = CheckSpaceEmpty();
            if (ischeckSpaceEmpty == true)
            {
                SetVariable();
                m_outputModel = m_calIns.CalAgeRate(m_InsModel, m_loanRentModel, m_outputModel);
                txtAge.Text = m_outputModel.ShowAge.ToString();
                txtRateInterestInsurance.Text = m_outputModel.ShowRateInsurance.ToString();
            }
        }

    }
}
