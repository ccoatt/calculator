﻿namespace Calculator
{
    partial class frmCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCash = new System.Windows.Forms.TextBox();
            this.TypeAgreement = new System.Windows.Forms.Label();
            this.lbCash = new System.Windows.Forms.Label();
            this.lbCashDown = new System.Windows.Forms.Label();
            this.lbCsahResult = new System.Windows.Forms.Label();
            this.lbTimeRentPerMonth = new System.Windows.Forms.Label();
            this.lbTotalRentCash = new System.Windows.Forms.Label();
            this.lbRateInterestRent = new System.Windows.Forms.Label();
            this.lbRateInterestRentTrue = new System.Windows.Forms.Label();
            this.txtCashDown = new System.Windows.Forms.TextBox();
            this.txtCashResult = new System.Windows.Forms.TextBox();
            this.txtTimeRentPerMonth = new System.Windows.Forms.TextBox();
            this.txtCashRentTotal = new System.Windows.Forms.TextBox();
            this.txtRateInterestRent = new System.Windows.Forms.TextBox();
            this.txtRateInterestRentTrue = new System.Windows.Forms.TextBox();
            this.btnCalculator = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.lbInterestTotal = new System.Windows.Forms.Label();
            this.txtInterestTotal = new System.Windows.Forms.TextBox();
            this.txtVat = new System.Windows.Forms.TextBox();
            this.lbVat = new System.Windows.Forms.Label();
            this.txtTotalCashWithInterest = new System.Windows.Forms.TextBox();
            this.lbTotalCashWithInterest = new System.Windows.Forms.Label();
            this.txtPaymentPerMonth = new System.Windows.Forms.TextBox();
            this.txtVatPerMonth = new System.Windows.Forms.TextBox();
            this.txtCashRentPerMonth = new System.Windows.Forms.TextBox();
            this.lbPaymentPerMonth = new System.Windows.Forms.Label();
            this.lbTexPerMonth = new System.Windows.Forms.Label();
            this.lbCashRentPerMonth = new System.Windows.Forms.Label();
            this.txtDiscountPerMonth = new System.Windows.Forms.TextBox();
            this.txtCashRentDiscountPerMonth = new System.Windows.Forms.TextBox();
            this.lbCashRentDiscountPerMonth = new System.Windows.Forms.Label();
            this.lbDiscountPerMonth = new System.Windows.Forms.Label();
            this.gbRent = new System.Windows.Forms.GroupBox();
            this.rdoRent = new System.Windows.Forms.RadioButton();
            this.rdoLoan = new System.Windows.Forms.RadioButton();
            this.lbPersent2 = new System.Windows.Forms.Label();
            this.lbPersent = new System.Windows.Forms.Label();
            this.lbBaht4 = new System.Windows.Forms.Label();
            this.lbMonth = new System.Windows.Forms.Label();
            this.lbBaht3 = new System.Windows.Forms.Label();
            this.lbBaht2 = new System.Windows.Forms.Label();
            this.lbBaht = new System.Windows.Forms.Label();
            this.gbInterestInsurance = new System.Windows.Forms.GroupBox();
            this.dtpBirthday = new System.Windows.Forms.DateTimePicker();
            this.cboGender = new System.Windows.Forms.ComboBox();
            this.rdoMTL = new System.Windows.Forms.RadioButton();
            this.rdoAIA = new System.Windows.Forms.RadioButton();
            this.lbPriceInsurance = new System.Windows.Forms.Label();
            this.lbRateInterestInsurance = new System.Windows.Forms.Label();
            this.lbSex = new System.Windows.Forms.Label();
            this.lbAge = new System.Windows.Forms.Label();
            this.lbYear = new System.Windows.Forms.Label();
            this.lbPersent3 = new System.Windows.Forms.Label();
            this.lbBaht5 = new System.Windows.Forms.Label();
            this.rdoNoInsurance = new System.Windows.Forms.RadioButton();
            this.lbInsurance = new System.Windows.Forms.Label();
            this.lbCompanyInsurance = new System.Windows.Forms.Label();
            this.lbBirthday = new System.Windows.Forms.Label();
            this.txtCompanyInsurance = new System.Windows.Forms.TextBox();
            this.txtPriceInsurance = new System.Windows.Forms.TextBox();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.txtRateInterestInsurance = new System.Windows.Forms.TextBox();
            this.lbBaht6 = new System.Windows.Forms.Label();
            this.lbBaht9 = new System.Windows.Forms.Label();
            this.lbBaht10 = new System.Windows.Forms.Label();
            this.lbBaht11 = new System.Windows.Forms.Label();
            this.lbBaht13 = new System.Windows.Forms.Label();
            this.lbBaht12 = new System.Windows.Forms.Label();
            this.lbBaht8 = new System.Windows.Forms.Label();
            this.lbBaht7 = new System.Windows.Forms.Label();
            this.gbRent.SuspendLayout();
            this.gbInterestInsurance.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtCash
            // 
            this.txtCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtCash.Location = new System.Drawing.Point(214, 49);
            this.txtCash.Name = "txtCash";
            this.txtCash.Size = new System.Drawing.Size(179, 26);
            this.txtCash.TabIndex = 1;
            this.txtCash.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCash_KeyPress);
            this.txtCash.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCash_KeyUp);
            this.txtCash.Leave += new System.EventHandler(this.txtCash_Leave);
            // 
            // TypeAgreement
            // 
            this.TypeAgreement.AutoSize = true;
            this.TypeAgreement.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.TypeAgreement.Location = new System.Drawing.Point(94, 19);
            this.TypeAgreement.Name = "TypeAgreement";
            this.TypeAgreement.Size = new System.Drawing.Size(96, 20);
            this.TypeAgreement.TabIndex = 1;
            this.TypeAgreement.Text = "ประเภทสัญญา";
            // 
            // lbCash
            // 
            this.lbCash.AutoSize = true;
            this.lbCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbCash.Location = new System.Drawing.Point(112, 55);
            this.lbCash.Name = "lbCash";
            this.lbCash.Size = new System.Drawing.Size(78, 20);
            this.lbCash.TabIndex = 1;
            this.lbCash.Text = "ราคาเงินสด";
            // 
            // lbCashDown
            // 
            this.lbCashDown.AutoSize = true;
            this.lbCashDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbCashDown.Location = new System.Drawing.Point(97, 91);
            this.lbCashDown.Name = "lbCashDown";
            this.lbCashDown.Size = new System.Drawing.Size(93, 20);
            this.lbCashDown.TabIndex = 1;
            this.lbCashDown.Text = "ราคาเงินดาวน์";
            // 
            // lbCsahResult
            // 
            this.lbCsahResult.AutoSize = true;
            this.lbCsahResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbCsahResult.Location = new System.Drawing.Point(48, 127);
            this.lbCsahResult.Name = "lbCsahResult";
            this.lbCsahResult.Size = new System.Drawing.Size(143, 20);
            this.lbCsahResult.TabIndex = 1;
            this.lbCsahResult.Text = "ราคาเงินสดส่วนที่เหลือ";
            // 
            // lbTimeRentPerMonth
            // 
            this.lbTimeRentPerMonth.AutoSize = true;
            this.lbTimeRentPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbTimeRentPerMonth.Location = new System.Drawing.Point(31, 163);
            this.lbTimeRentPerMonth.Name = "lbTimeRentPerMonth";
            this.lbTimeRentPerMonth.Size = new System.Drawing.Size(159, 20);
            this.lbTimeRentPerMonth.TabIndex = 1;
            this.lbTimeRentPerMonth.Text = "ระยะเวลาเช่าซื้อรายเดือน";
            // 
            // lbTotalRentCash
            // 
            this.lbTotalRentCash.AutoSize = true;
            this.lbTotalRentCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbTotalRentCash.Location = new System.Drawing.Point(92, 199);
            this.lbTotalRentCash.Name = "lbTotalRentCash";
            this.lbTotalRentCash.Size = new System.Drawing.Size(98, 20);
            this.lbTotalRentCash.TabIndex = 1;
            this.lbTotalRentCash.Text = "ค่าเช่าซื้อทั้งสิ้น";
            // 
            // lbRateInterestRent
            // 
            this.lbRateInterestRent.AutoSize = true;
            this.lbRateInterestRent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbRateInterestRent.Location = new System.Drawing.Point(62, 235);
            this.lbRateInterestRent.Name = "lbRateInterestRent";
            this.lbRateInterestRent.Size = new System.Drawing.Size(128, 20);
            this.lbRateInterestRent.TabIndex = 1;
            this.lbRateInterestRent.Text = "อัคราดอกเบี้ยเช่าซื้อ";
            // 
            // lbRateInterestRentTrue
            // 
            this.lbRateInterestRentTrue.AutoSize = true;
            this.lbRateInterestRentTrue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbRateInterestRentTrue.Location = new System.Drawing.Point(11, 271);
            this.lbRateInterestRentTrue.Name = "lbRateInterestRentTrue";
            this.lbRateInterestRentTrue.Size = new System.Drawing.Size(179, 20);
            this.lbRateInterestRentTrue.TabIndex = 1;
            this.lbRateInterestRentTrue.Text = "อัตราดอกเบี้ยเช่าซื้อตรงเวลา";
            // 
            // txtCashDown
            // 
            this.txtCashDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtCashDown.Location = new System.Drawing.Point(214, 86);
            this.txtCashDown.Name = "txtCashDown";
            this.txtCashDown.Size = new System.Drawing.Size(179, 26);
            this.txtCashDown.TabIndex = 2;
            this.txtCashDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashDown.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCashDown_KeyPress);
            this.txtCashDown.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCashDown_KeyUp);
            this.txtCashDown.Leave += new System.EventHandler(this.txtCashDown_Leave);
            // 
            // txtCashResult
            // 
            this.txtCashResult.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCashResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtCashResult.Location = new System.Drawing.Point(214, 123);
            this.txtCashResult.Name = "txtCashResult";
            this.txtCashResult.ReadOnly = true;
            this.txtCashResult.Size = new System.Drawing.Size(179, 26);
            this.txtCashResult.TabIndex = 99;
            this.txtCashResult.TabStop = false;
            this.txtCashResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTimeRentPerMonth
            // 
            this.txtTimeRentPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtTimeRentPerMonth.Location = new System.Drawing.Point(214, 160);
            this.txtTimeRentPerMonth.Name = "txtTimeRentPerMonth";
            this.txtTimeRentPerMonth.Size = new System.Drawing.Size(179, 26);
            this.txtTimeRentPerMonth.TabIndex = 3;
            this.txtTimeRentPerMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTimeRentPerMonth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTimeRentPerMonth_KeyPress);
            // 
            // txtCashRentTotal
            // 
            this.txtCashRentTotal.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCashRentTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtCashRentTotal.Location = new System.Drawing.Point(214, 197);
            this.txtCashRentTotal.Name = "txtCashRentTotal";
            this.txtCashRentTotal.ReadOnly = true;
            this.txtCashRentTotal.Size = new System.Drawing.Size(179, 26);
            this.txtCashRentTotal.TabIndex = 60;
            this.txtCashRentTotal.TabStop = false;
            this.txtCashRentTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRateInterestRent
            // 
            this.txtRateInterestRent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtRateInterestRent.Location = new System.Drawing.Point(214, 234);
            this.txtRateInterestRent.Name = "txtRateInterestRent";
            this.txtRateInterestRent.Size = new System.Drawing.Size(179, 26);
            this.txtRateInterestRent.TabIndex = 4;
            this.txtRateInterestRent.Text = "15";
            this.txtRateInterestRent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRateInterestRent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRateInterestRent_KeyPress);
            // 
            // txtRateInterestRentTrue
            // 
            this.txtRateInterestRentTrue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtRateInterestRentTrue.Location = new System.Drawing.Point(214, 271);
            this.txtRateInterestRentTrue.Name = "txtRateInterestRentTrue";
            this.txtRateInterestRentTrue.Size = new System.Drawing.Size(179, 26);
            this.txtRateInterestRentTrue.TabIndex = 5;
            this.txtRateInterestRentTrue.Text = "12";
            this.txtRateInterestRentTrue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRateInterestRentTrue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRateInterestRentTrue_KeyPress);
            // 
            // btnCalculator
            // 
            this.btnCalculator.BackColor = System.Drawing.Color.MediumBlue;
            this.btnCalculator.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCalculator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnCalculator.ForeColor = System.Drawing.Color.GhostWhite;
            this.btnCalculator.Location = new System.Drawing.Point(122, 310);
            this.btnCalculator.Name = "btnCalculator";
            this.btnCalculator.Size = new System.Drawing.Size(100, 32);
            this.btnCalculator.TabIndex = 6;
            this.btnCalculator.Text = "คำนวณ";
            this.btnCalculator.UseVisualStyleBackColor = false;
            this.btnCalculator.Click += new System.EventHandler(this.btnCalculator_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.Chocolate;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnReset.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnReset.Location = new System.Drawing.Point(251, 310);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(100, 32);
            this.btnReset.TabIndex = 7;
            this.btnReset.Text = "คืนค่า";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lbInterestTotal
            // 
            this.lbInterestTotal.AutoSize = true;
            this.lbInterestTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbInterestTotal.Location = new System.Drawing.Point(553, 41);
            this.lbInterestTotal.Name = "lbInterestTotal";
            this.lbInterestTotal.Size = new System.Drawing.Size(129, 18);
            this.lbInterestTotal.TabIndex = 1;
            this.lbInterestTotal.Text = "ดอกเบี้ยเช่าซื้อทั้งหมด";
            // 
            // txtInterestTotal
            // 
            this.txtInterestTotal.BackColor = System.Drawing.Color.Gainsboro;
            this.txtInterestTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtInterestTotal.Location = new System.Drawing.Point(557, 64);
            this.txtInterestTotal.Name = "txtInterestTotal";
            this.txtInterestTotal.ReadOnly = true;
            this.txtInterestTotal.Size = new System.Drawing.Size(174, 26);
            this.txtInterestTotal.TabIndex = 99;
            this.txtInterestTotal.TabStop = false;
            this.txtInterestTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtVat
            // 
            this.txtVat.BackColor = System.Drawing.Color.Gainsboro;
            this.txtVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtVat.Location = new System.Drawing.Point(557, 120);
            this.txtVat.Name = "txtVat";
            this.txtVat.ReadOnly = true;
            this.txtVat.Size = new System.Drawing.Size(174, 26);
            this.txtVat.TabIndex = 99;
            this.txtVat.TabStop = false;
            this.txtVat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbVat
            // 
            this.lbVat.AutoSize = true;
            this.lbVat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbVat.Location = new System.Drawing.Point(553, 97);
            this.lbVat.Name = "lbVat";
            this.lbVat.Size = new System.Drawing.Size(54, 18);
            this.lbVat.TabIndex = 1;
            this.lbVat.Text = "Vat 7%";
            // 
            // txtTotalCashWithInterest
            // 
            this.txtTotalCashWithInterest.BackColor = System.Drawing.Color.Gainsboro;
            this.txtTotalCashWithInterest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtTotalCashWithInterest.Location = new System.Drawing.Point(557, 176);
            this.txtTotalCashWithInterest.Name = "txtTotalCashWithInterest";
            this.txtTotalCashWithInterest.ReadOnly = true;
            this.txtTotalCashWithInterest.Size = new System.Drawing.Size(174, 26);
            this.txtTotalCashWithInterest.TabIndex = 99;
            this.txtTotalCashWithInterest.TabStop = false;
            this.txtTotalCashWithInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbTotalCashWithInterest
            // 
            this.lbTotalCashWithInterest.AutoSize = true;
            this.lbTotalCashWithInterest.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbTotalCashWithInterest.Location = new System.Drawing.Point(553, 153);
            this.lbTotalCashWithInterest.Name = "lbTotalCashWithInterest";
            this.lbTotalCashWithInterest.Size = new System.Drawing.Size(193, 18);
            this.lbTotalCashWithInterest.TabIndex = 1;
            this.lbTotalCashWithInterest.Text = "ค่าเช่าซื้อรวมดอกเบี้ย(ทุนประกัน)";
            // 
            // txtPaymentPerMonth
            // 
            this.txtPaymentPerMonth.BackColor = System.Drawing.Color.Gainsboro;
            this.txtPaymentPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtPaymentPerMonth.Location = new System.Drawing.Point(557, 306);
            this.txtPaymentPerMonth.Name = "txtPaymentPerMonth";
            this.txtPaymentPerMonth.ReadOnly = true;
            this.txtPaymentPerMonth.Size = new System.Drawing.Size(178, 26);
            this.txtPaymentPerMonth.TabIndex = 99;
            this.txtPaymentPerMonth.TabStop = false;
            this.txtPaymentPerMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtVatPerMonth
            // 
            this.txtVatPerMonth.BackColor = System.Drawing.Color.Gainsboro;
            this.txtVatPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtVatPerMonth.Location = new System.Drawing.Point(557, 362);
            this.txtVatPerMonth.Name = "txtVatPerMonth";
            this.txtVatPerMonth.ReadOnly = true;
            this.txtVatPerMonth.Size = new System.Drawing.Size(178, 26);
            this.txtVatPerMonth.TabIndex = 99;
            this.txtVatPerMonth.TabStop = false;
            this.txtVatPerMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCashRentPerMonth
            // 
            this.txtCashRentPerMonth.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCashRentPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtCashRentPerMonth.Location = new System.Drawing.Point(557, 418);
            this.txtCashRentPerMonth.Name = "txtCashRentPerMonth";
            this.txtCashRentPerMonth.ReadOnly = true;
            this.txtCashRentPerMonth.Size = new System.Drawing.Size(178, 26);
            this.txtCashRentPerMonth.TabIndex = 99;
            this.txtCashRentPerMonth.TabStop = false;
            this.txtCashRentPerMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbPaymentPerMonth
            // 
            this.lbPaymentPerMonth.AutoSize = true;
            this.lbPaymentPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbPaymentPerMonth.Location = new System.Drawing.Point(553, 283);
            this.lbPaymentPerMonth.Name = "lbPaymentPerMonth";
            this.lbPaymentPerMonth.Size = new System.Drawing.Size(76, 18);
            this.lbPaymentPerMonth.TabIndex = 1;
            this.lbPaymentPerMonth.Text = "ค่างวด/เดือน";
            // 
            // lbTexPerMonth
            // 
            this.lbTexPerMonth.AutoSize = true;
            this.lbTexPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbTexPerMonth.Location = new System.Drawing.Point(553, 339);
            this.lbTexPerMonth.Name = "lbTexPerMonth";
            this.lbTexPerMonth.Size = new System.Drawing.Size(65, 18);
            this.lbTexPerMonth.TabIndex = 1;
            this.lbTexPerMonth.Text = "ภาษี/เดือน";
            // 
            // lbCashRentPerMonth
            // 
            this.lbCashRentPerMonth.AutoSize = true;
            this.lbCashRentPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbCashRentPerMonth.Location = new System.Drawing.Point(553, 395);
            this.lbCashRentPerMonth.Name = "lbCashRentPerMonth";
            this.lbCashRentPerMonth.Size = new System.Drawing.Size(90, 18);
            this.lbCashRentPerMonth.TabIndex = 1;
            this.lbCashRentPerMonth.Text = "ค่าเช่าซื้อ/เดือน";
            // 
            // txtDiscountPerMonth
            // 
            this.txtDiscountPerMonth.BackColor = System.Drawing.Color.Gainsboro;
            this.txtDiscountPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtDiscountPerMonth.Location = new System.Drawing.Point(556, 557);
            this.txtDiscountPerMonth.Name = "txtDiscountPerMonth";
            this.txtDiscountPerMonth.ReadOnly = true;
            this.txtDiscountPerMonth.Size = new System.Drawing.Size(174, 26);
            this.txtDiscountPerMonth.TabIndex = 99;
            this.txtDiscountPerMonth.TabStop = false;
            this.txtDiscountPerMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCashRentDiscountPerMonth
            // 
            this.txtCashRentDiscountPerMonth.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCashRentDiscountPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtCashRentDiscountPerMonth.Location = new System.Drawing.Point(557, 624);
            this.txtCashRentDiscountPerMonth.Name = "txtCashRentDiscountPerMonth";
            this.txtCashRentDiscountPerMonth.ReadOnly = true;
            this.txtCashRentDiscountPerMonth.Size = new System.Drawing.Size(174, 26);
            this.txtCashRentDiscountPerMonth.TabIndex = 99;
            this.txtCashRentDiscountPerMonth.TabStop = false;
            this.txtCashRentDiscountPerMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbCashRentDiscountPerMonth
            // 
            this.lbCashRentDiscountPerMonth.AutoSize = true;
            this.lbCashRentDiscountPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbCashRentDiscountPerMonth.Location = new System.Drawing.Point(553, 601);
            this.lbCashRentDiscountPerMonth.Name = "lbCashRentDiscountPerMonth";
            this.lbCashRentDiscountPerMonth.Size = new System.Drawing.Size(129, 18);
            this.lbCashRentDiscountPerMonth.TabIndex = 1;
            this.lbCashRentDiscountPerMonth.Text = "ค่าเช่าซื้อส่วนลด/เดือน";
            // 
            // lbDiscountPerMonth
            // 
            this.lbDiscountPerMonth.AutoSize = true;
            this.lbDiscountPerMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbDiscountPerMonth.Location = new System.Drawing.Point(552, 534);
            this.lbDiscountPerMonth.Name = "lbDiscountPerMonth";
            this.lbDiscountPerMonth.Size = new System.Drawing.Size(80, 18);
            this.lbDiscountPerMonth.TabIndex = 1;
            this.lbDiscountPerMonth.Text = "ส่วนลด/เดือน";
            // 
            // gbRent
            // 
            this.gbRent.BackColor = System.Drawing.Color.AliceBlue;
            this.gbRent.Controls.Add(this.rdoRent);
            this.gbRent.Controls.Add(this.txtRateInterestRentTrue);
            this.gbRent.Controls.Add(this.rdoLoan);
            this.gbRent.Controls.Add(this.txtRateInterestRent);
            this.gbRent.Controls.Add(this.btnReset);
            this.gbRent.Controls.Add(this.txtCashRentTotal);
            this.gbRent.Controls.Add(this.btnCalculator);
            this.gbRent.Controls.Add(this.txtTimeRentPerMonth);
            this.gbRent.Controls.Add(this.lbPersent2);
            this.gbRent.Controls.Add(this.txtCashResult);
            this.gbRent.Controls.Add(this.lbPersent);
            this.gbRent.Controls.Add(this.lbBaht4);
            this.gbRent.Controls.Add(this.lbMonth);
            this.gbRent.Controls.Add(this.lbBaht3);
            this.gbRent.Controls.Add(this.lbBaht2);
            this.gbRent.Controls.Add(this.lbBaht);
            this.gbRent.Controls.Add(this.TypeAgreement);
            this.gbRent.Controls.Add(this.lbCash);
            this.gbRent.Controls.Add(this.lbCashDown);
            this.gbRent.Controls.Add(this.txtCashDown);
            this.gbRent.Controls.Add(this.lbCsahResult);
            this.gbRent.Controls.Add(this.txtCash);
            this.gbRent.Controls.Add(this.lbTimeRentPerMonth);
            this.gbRent.Controls.Add(this.lbTotalRentCash);
            this.gbRent.Controls.Add(this.lbRateInterestRent);
            this.gbRent.Controls.Add(this.lbRateInterestRentTrue);
            this.gbRent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.gbRent.Location = new System.Drawing.Point(12, 12);
            this.gbRent.Name = "gbRent";
            this.gbRent.Size = new System.Drawing.Size(506, 354);
            this.gbRent.TabIndex = 3;
            this.gbRent.TabStop = false;
            this.gbRent.Text = "เงื่อนไขเช่าซื้อ";
            // 
            // rdoRent
            // 
            this.rdoRent.AutoSize = true;
            this.rdoRent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.rdoRent.Location = new System.Drawing.Point(313, 17);
            this.rdoRent.Name = "rdoRent";
            this.rdoRent.Size = new System.Drawing.Size(66, 24);
            this.rdoRent.TabIndex = 44;
            this.rdoRent.Text = "เช่าซื้อ";
            this.rdoRent.UseVisualStyleBackColor = true;
            // 
            // rdoLoan
            // 
            this.rdoLoan.AutoSize = true;
            this.rdoLoan.Checked = true;
            this.rdoLoan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.rdoLoan.Location = new System.Drawing.Point(235, 17);
            this.rdoLoan.Name = "rdoLoan";
            this.rdoLoan.Size = new System.Drawing.Size(58, 24);
            this.rdoLoan.TabIndex = 44;
            this.rdoLoan.TabStop = true;
            this.rdoLoan.Text = "เงินกู้";
            this.rdoLoan.UseVisualStyleBackColor = true;
            // 
            // lbPersent2
            // 
            this.lbPersent2.AutoSize = true;
            this.lbPersent2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbPersent2.Location = new System.Drawing.Point(405, 272);
            this.lbPersent2.Name = "lbPersent2";
            this.lbPersent2.Size = new System.Drawing.Size(49, 20);
            this.lbPersent2.TabIndex = 1;
            this.lbPersent2.Text = "%ต่อปี";
            // 
            // lbPersent
            // 
            this.lbPersent.AutoSize = true;
            this.lbPersent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbPersent.Location = new System.Drawing.Point(405, 236);
            this.lbPersent.Name = "lbPersent";
            this.lbPersent.Size = new System.Drawing.Size(49, 20);
            this.lbPersent.TabIndex = 1;
            this.lbPersent.Text = "%ต่อปี";
            // 
            // lbBaht4
            // 
            this.lbBaht4.AutoSize = true;
            this.lbBaht4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht4.Location = new System.Drawing.Point(405, 200);
            this.lbBaht4.Name = "lbBaht4";
            this.lbBaht4.Size = new System.Drawing.Size(35, 20);
            this.lbBaht4.TabIndex = 1;
            this.lbBaht4.Text = "บาท";
            // 
            // lbMonth
            // 
            this.lbMonth.AutoSize = true;
            this.lbMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbMonth.Location = new System.Drawing.Point(405, 164);
            this.lbMonth.Name = "lbMonth";
            this.lbMonth.Size = new System.Drawing.Size(41, 20);
            this.lbMonth.TabIndex = 1;
            this.lbMonth.Text = "เดือน";
            // 
            // lbBaht3
            // 
            this.lbBaht3.AutoSize = true;
            this.lbBaht3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht3.Location = new System.Drawing.Point(405, 128);
            this.lbBaht3.Name = "lbBaht3";
            this.lbBaht3.Size = new System.Drawing.Size(35, 20);
            this.lbBaht3.TabIndex = 1;
            this.lbBaht3.Text = "บาท";
            // 
            // lbBaht2
            // 
            this.lbBaht2.AutoSize = true;
            this.lbBaht2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht2.Location = new System.Drawing.Point(405, 92);
            this.lbBaht2.Name = "lbBaht2";
            this.lbBaht2.Size = new System.Drawing.Size(35, 20);
            this.lbBaht2.TabIndex = 1;
            this.lbBaht2.Text = "บาท";
            // 
            // lbBaht
            // 
            this.lbBaht.AutoSize = true;
            this.lbBaht.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht.Location = new System.Drawing.Point(405, 56);
            this.lbBaht.Name = "lbBaht";
            this.lbBaht.Size = new System.Drawing.Size(35, 20);
            this.lbBaht.TabIndex = 1;
            this.lbBaht.Text = "บาท";
            // 
            // gbInterestInsurance
            // 
            this.gbInterestInsurance.BackColor = System.Drawing.Color.AliceBlue;
            this.gbInterestInsurance.Controls.Add(this.dtpBirthday);
            this.gbInterestInsurance.Controls.Add(this.cboGender);
            this.gbInterestInsurance.Controls.Add(this.rdoMTL);
            this.gbInterestInsurance.Controls.Add(this.rdoAIA);
            this.gbInterestInsurance.Controls.Add(this.lbPriceInsurance);
            this.gbInterestInsurance.Controls.Add(this.lbRateInterestInsurance);
            this.gbInterestInsurance.Controls.Add(this.lbSex);
            this.gbInterestInsurance.Controls.Add(this.lbAge);
            this.gbInterestInsurance.Controls.Add(this.lbYear);
            this.gbInterestInsurance.Controls.Add(this.lbPersent3);
            this.gbInterestInsurance.Controls.Add(this.lbBaht5);
            this.gbInterestInsurance.Controls.Add(this.rdoNoInsurance);
            this.gbInterestInsurance.Controls.Add(this.lbInsurance);
            this.gbInterestInsurance.Controls.Add(this.lbCompanyInsurance);
            this.gbInterestInsurance.Controls.Add(this.lbBirthday);
            this.gbInterestInsurance.Controls.Add(this.txtCompanyInsurance);
            this.gbInterestInsurance.Controls.Add(this.txtPriceInsurance);
            this.gbInterestInsurance.Controls.Add(this.txtAge);
            this.gbInterestInsurance.Controls.Add(this.txtRateInterestInsurance);
            this.gbInterestInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.gbInterestInsurance.Location = new System.Drawing.Point(12, 372);
            this.gbInterestInsurance.Name = "gbInterestInsurance";
            this.gbInterestInsurance.Size = new System.Drawing.Size(506, 287);
            this.gbInterestInsurance.TabIndex = 4;
            this.gbInterestInsurance.TabStop = false;
            this.gbInterestInsurance.Text = "อัตราดอกเบี้ยประกัน";
            // 
            // dtpBirthday
            // 
            this.dtpBirthday.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.dtpBirthday.Location = new System.Drawing.Point(172, 97);
            this.dtpBirthday.MaxDate = new System.DateTime(2017, 7, 3, 0, 0, 0, 0);
            this.dtpBirthday.Name = "dtpBirthday";
            this.dtpBirthday.Size = new System.Drawing.Size(179, 22);
            this.dtpBirthday.TabIndex = 11;
            this.dtpBirthday.Value = new System.DateTime(2017, 7, 3, 0, 0, 0, 0);
            this.dtpBirthday.ValueChanged += new System.EventHandler(this.dtpBirthday_ValueChanged);
            // 
            // cboGender
            // 
            this.cboGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.cboGender.FormattingEnabled = true;
            this.cboGender.Items.AddRange(new object[] {
            "หญิง",
            "ชาย"});
            this.cboGender.Location = new System.Drawing.Point(172, 166);
            this.cboGender.Name = "cboGender";
            this.cboGender.Size = new System.Drawing.Size(179, 28);
            this.cboGender.TabIndex = 13;
            this.cboGender.Text = "ชาย";
            this.cboGender.SelectedValueChanged += new System.EventHandler(this.cboSex_SelectedValueChanged);
            // 
            // rdoMTL
            // 
            this.rdoMTL.AutoSize = true;
            this.rdoMTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.rdoMTL.Location = new System.Drawing.Point(340, 22);
            this.rdoMTL.Name = "rdoMTL";
            this.rdoMTL.Size = new System.Drawing.Size(58, 24);
            this.rdoMTL.TabIndex = 10;
            this.rdoMTL.TabStop = true;
            this.rdoMTL.Text = "MTL";
            this.rdoMTL.UseVisualStyleBackColor = true;
            this.rdoMTL.Click += new System.EventHandler(this.rdoMTL_Click);
            // 
            // rdoAIA
            // 
            this.rdoAIA.AutoSize = true;
            this.rdoAIA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.rdoAIA.Location = new System.Drawing.Point(280, 22);
            this.rdoAIA.Name = "rdoAIA";
            this.rdoAIA.Size = new System.Drawing.Size(54, 24);
            this.rdoAIA.TabIndex = 9;
            this.rdoAIA.TabStop = true;
            this.rdoAIA.Text = "AIA";
            this.rdoAIA.UseVisualStyleBackColor = true;
            this.rdoAIA.Click += new System.EventHandler(this.rdoAIA_Click);
            // 
            // lbPriceInsurance
            // 
            this.lbPriceInsurance.AutoSize = true;
            this.lbPriceInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbPriceInsurance.Location = new System.Drawing.Point(77, 248);
            this.lbPriceInsurance.Name = "lbPriceInsurance";
            this.lbPriceInsurance.Size = new System.Drawing.Size(89, 20);
            this.lbPriceInsurance.TabIndex = 1;
            this.lbPriceInsurance.Text = "ค่าเบี้ยประกัน";
            // 
            // lbRateInterestInsurance
            // 
            this.lbRateInterestInsurance.AutoSize = true;
            this.lbRateInterestInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbRateInterestInsurance.Location = new System.Drawing.Point(62, 208);
            this.lbRateInterestInsurance.Name = "lbRateInterestInsurance";
            this.lbRateInterestInsurance.Size = new System.Drawing.Size(104, 20);
            this.lbRateInterestInsurance.TabIndex = 1;
            this.lbRateInterestInsurance.Text = "อัตราเบี้ยประกัน";
            // 
            // lbSex
            // 
            this.lbSex.AutoSize = true;
            this.lbSex.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbSex.Location = new System.Drawing.Point(132, 170);
            this.lbSex.Name = "lbSex";
            this.lbSex.Size = new System.Drawing.Size(34, 20);
            this.lbSex.TabIndex = 1;
            this.lbSex.Text = "เพศ";
            // 
            // lbAge
            // 
            this.lbAge.AutoSize = true;
            this.lbAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbAge.Location = new System.Drawing.Point(134, 133);
            this.lbAge.Name = "lbAge";
            this.lbAge.Size = new System.Drawing.Size(32, 20);
            this.lbAge.TabIndex = 1;
            this.lbAge.Text = "อายุ";
            // 
            // lbYear
            // 
            this.lbYear.AutoSize = true;
            this.lbYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbYear.Location = new System.Drawing.Point(357, 135);
            this.lbYear.Name = "lbYear";
            this.lbYear.Size = new System.Drawing.Size(18, 20);
            this.lbYear.TabIndex = 1;
            this.lbYear.Text = "ปี";
            // 
            // lbPersent3
            // 
            this.lbPersent3.AutoSize = true;
            this.lbPersent3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbPersent3.Location = new System.Drawing.Point(357, 209);
            this.lbPersent3.Name = "lbPersent3";
            this.lbPersent3.Size = new System.Drawing.Size(23, 20);
            this.lbPersent3.TabIndex = 1;
            this.lbPersent3.Text = "%";
            // 
            // lbBaht5
            // 
            this.lbBaht5.AutoSize = true;
            this.lbBaht5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht5.Location = new System.Drawing.Point(357, 252);
            this.lbBaht5.Name = "lbBaht5";
            this.lbBaht5.Size = new System.Drawing.Size(35, 20);
            this.lbBaht5.TabIndex = 1;
            this.lbBaht5.Text = "บาท";
            // 
            // rdoNoInsurance
            // 
            this.rdoNoInsurance.AutoSize = true;
            this.rdoNoInsurance.Checked = true;
            this.rdoNoInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.rdoNoInsurance.Location = new System.Drawing.Point(172, 22);
            this.rdoNoInsurance.Name = "rdoNoInsurance";
            this.rdoNoInsurance.Size = new System.Drawing.Size(102, 24);
            this.rdoNoInsurance.TabIndex = 8;
            this.rdoNoInsurance.TabStop = true;
            this.rdoNoInsurance.Text = "ไม่ทำประกัน";
            this.rdoNoInsurance.UseVisualStyleBackColor = true;
            // 
            // lbInsurance
            // 
            this.lbInsurance.AutoSize = true;
            this.lbInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbInsurance.Location = new System.Drawing.Point(93, 22);
            this.lbInsurance.Name = "lbInsurance";
            this.lbInsurance.Size = new System.Drawing.Size(73, 20);
            this.lbInsurance.TabIndex = 1;
            this.lbInsurance.Text = "เบี้ยประกัน";
            // 
            // lbCompanyInsurance
            // 
            this.lbCompanyInsurance.AutoSize = true;
            this.lbCompanyInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbCompanyInsurance.Location = new System.Drawing.Point(81, 62);
            this.lbCompanyInsurance.Name = "lbCompanyInsurance";
            this.lbCompanyInsurance.Size = new System.Drawing.Size(85, 20);
            this.lbCompanyInsurance.TabIndex = 1;
            this.lbCompanyInsurance.Text = "บริษัทประกัน";
            // 
            // lbBirthday
            // 
            this.lbBirthday.AutoSize = true;
            this.lbBirthday.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBirthday.Location = new System.Drawing.Point(117, 97);
            this.lbBirthday.Name = "lbBirthday";
            this.lbBirthday.Size = new System.Drawing.Size(49, 20);
            this.lbBirthday.TabIndex = 1;
            this.lbBirthday.Text = "วันเกิด";
            // 
            // txtCompanyInsurance
            // 
            this.txtCompanyInsurance.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCompanyInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtCompanyInsurance.Location = new System.Drawing.Point(172, 58);
            this.txtCompanyInsurance.Name = "txtCompanyInsurance";
            this.txtCompanyInsurance.ReadOnly = true;
            this.txtCompanyInsurance.Size = new System.Drawing.Size(179, 26);
            this.txtCompanyInsurance.TabIndex = 99;
            this.txtCompanyInsurance.TabStop = false;
            this.txtCompanyInsurance.Text = "ไม่ทำประกัน";
            this.txtCompanyInsurance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPriceInsurance
            // 
            this.txtPriceInsurance.BackColor = System.Drawing.Color.Gainsboro;
            this.txtPriceInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtPriceInsurance.Location = new System.Drawing.Point(172, 246);
            this.txtPriceInsurance.Name = "txtPriceInsurance";
            this.txtPriceInsurance.ReadOnly = true;
            this.txtPriceInsurance.Size = new System.Drawing.Size(179, 26);
            this.txtPriceInsurance.TabIndex = 60;
            this.txtPriceInsurance.TabStop = false;
            this.txtPriceInsurance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAge
            // 
            this.txtAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtAge.Location = new System.Drawing.Point(172, 130);
            this.txtAge.Name = "txtAge";
            this.txtAge.ReadOnly = true;
            this.txtAge.Size = new System.Drawing.Size(179, 26);
            this.txtAge.TabIndex = 14;
            this.txtAge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRateInterestInsurance_KeyPress);
            // 
            // txtRateInterestInsurance
            // 
            this.txtRateInterestInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtRateInterestInsurance.Location = new System.Drawing.Point(172, 206);
            this.txtRateInterestInsurance.Name = "txtRateInterestInsurance";
            this.txtRateInterestInsurance.ReadOnly = true;
            this.txtRateInterestInsurance.Size = new System.Drawing.Size(179, 26);
            this.txtRateInterestInsurance.TabIndex = 14;
            this.txtRateInterestInsurance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRateInterestInsurance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRateInterestInsurance_KeyPress);
            // 
            // lbBaht6
            // 
            this.lbBaht6.AutoSize = true;
            this.lbBaht6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht6.Location = new System.Drawing.Point(737, 67);
            this.lbBaht6.Name = "lbBaht6";
            this.lbBaht6.Size = new System.Drawing.Size(35, 20);
            this.lbBaht6.TabIndex = 1;
            this.lbBaht6.Text = "บาท";
            // 
            // lbBaht9
            // 
            this.lbBaht9.AutoSize = true;
            this.lbBaht9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht9.Location = new System.Drawing.Point(741, 309);
            this.lbBaht9.Name = "lbBaht9";
            this.lbBaht9.Size = new System.Drawing.Size(35, 20);
            this.lbBaht9.TabIndex = 1;
            this.lbBaht9.Text = "บาท";
            // 
            // lbBaht10
            // 
            this.lbBaht10.AutoSize = true;
            this.lbBaht10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht10.Location = new System.Drawing.Point(741, 368);
            this.lbBaht10.Name = "lbBaht10";
            this.lbBaht10.Size = new System.Drawing.Size(35, 20);
            this.lbBaht10.TabIndex = 1;
            this.lbBaht10.Text = "บาท";
            // 
            // lbBaht11
            // 
            this.lbBaht11.AutoSize = true;
            this.lbBaht11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht11.Location = new System.Drawing.Point(741, 418);
            this.lbBaht11.Name = "lbBaht11";
            this.lbBaht11.Size = new System.Drawing.Size(35, 20);
            this.lbBaht11.TabIndex = 1;
            this.lbBaht11.Text = "บาท";
            // 
            // lbBaht13
            // 
            this.lbBaht13.AutoSize = true;
            this.lbBaht13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht13.Location = new System.Drawing.Point(735, 627);
            this.lbBaht13.Name = "lbBaht13";
            this.lbBaht13.Size = new System.Drawing.Size(35, 20);
            this.lbBaht13.TabIndex = 1;
            this.lbBaht13.Text = "บาท";
            // 
            // lbBaht12
            // 
            this.lbBaht12.AutoSize = true;
            this.lbBaht12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht12.Location = new System.Drawing.Point(736, 559);
            this.lbBaht12.Name = "lbBaht12";
            this.lbBaht12.Size = new System.Drawing.Size(35, 20);
            this.lbBaht12.TabIndex = 1;
            this.lbBaht12.Text = "บาท";
            // 
            // lbBaht8
            // 
            this.lbBaht8.AutoSize = true;
            this.lbBaht8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht8.Location = new System.Drawing.Point(737, 179);
            this.lbBaht8.Name = "lbBaht8";
            this.lbBaht8.Size = new System.Drawing.Size(35, 20);
            this.lbBaht8.TabIndex = 1;
            this.lbBaht8.Text = "บาท";
            // 
            // lbBaht7
            // 
            this.lbBaht7.AutoSize = true;
            this.lbBaht7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbBaht7.Location = new System.Drawing.Point(737, 123);
            this.lbBaht7.Name = "lbBaht7";
            this.lbBaht7.Size = new System.Drawing.Size(35, 20);
            this.lbBaht7.TabIndex = 1;
            this.lbBaht7.Text = "บาท";
            // 
            // frmCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MintCream;
            this.ClientSize = new System.Drawing.Size(810, 671);
            this.Controls.Add(this.gbInterestInsurance);
            this.Controls.Add(this.lbBaht7);
            this.Controls.Add(this.lbBaht8);
            this.Controls.Add(this.lbBaht12);
            this.Controls.Add(this.lbBaht13);
            this.Controls.Add(this.lbBaht11);
            this.Controls.Add(this.lbBaht10);
            this.Controls.Add(this.lbBaht9);
            this.Controls.Add(this.lbBaht6);
            this.Controls.Add(this.lbCashRentDiscountPerMonth);
            this.Controls.Add(this.lbCashRentPerMonth);
            this.Controls.Add(this.lbTotalCashWithInterest);
            this.Controls.Add(this.lbDiscountPerMonth);
            this.Controls.Add(this.lbTexPerMonth);
            this.Controls.Add(this.lbVat);
            this.Controls.Add(this.lbPaymentPerMonth);
            this.Controls.Add(this.lbInterestTotal);
            this.Controls.Add(this.txtCashRentDiscountPerMonth);
            this.Controls.Add(this.txtCashRentPerMonth);
            this.Controls.Add(this.txtTotalCashWithInterest);
            this.Controls.Add(this.txtDiscountPerMonth);
            this.Controls.Add(this.txtVatPerMonth);
            this.Controls.Add(this.txtPaymentPerMonth);
            this.Controls.Add(this.txtVat);
            this.Controls.Add(this.txtInterestTotal);
            this.Controls.Add(this.gbRent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(826, 710);
            this.MinimizeBox = false;
            this.Name = "frmCalculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator";
            this.Load += new System.EventHandler(this.CalculatorForm_Load);
            this.gbRent.ResumeLayout(false);
            this.gbRent.PerformLayout();
            this.gbInterestInsurance.ResumeLayout(false);
            this.gbInterestInsurance.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCash;
        private System.Windows.Forms.Label TypeAgreement;
        private System.Windows.Forms.Label lbCash;
        private System.Windows.Forms.Label lbCashDown;
        private System.Windows.Forms.Label lbCsahResult;
        private System.Windows.Forms.Label lbTimeRentPerMonth;
        private System.Windows.Forms.Label lbTotalRentCash;
        private System.Windows.Forms.Label lbRateInterestRent;
        private System.Windows.Forms.Label lbRateInterestRentTrue;
        private System.Windows.Forms.TextBox txtCashDown;
        private System.Windows.Forms.TextBox txtCashResult;
        private System.Windows.Forms.TextBox txtTimeRentPerMonth;
        private System.Windows.Forms.TextBox txtCashRentTotal;
        private System.Windows.Forms.TextBox txtRateInterestRent;
        private System.Windows.Forms.TextBox txtRateInterestRentTrue;
        private System.Windows.Forms.Button btnCalculator;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label lbInterestTotal;
        private System.Windows.Forms.TextBox txtInterestTotal;
        private System.Windows.Forms.TextBox txtVat;
        private System.Windows.Forms.Label lbVat;
        private System.Windows.Forms.TextBox txtTotalCashWithInterest;
        private System.Windows.Forms.Label lbTotalCashWithInterest;
        private System.Windows.Forms.TextBox txtPaymentPerMonth;
        private System.Windows.Forms.TextBox txtVatPerMonth;
        private System.Windows.Forms.TextBox txtCashRentPerMonth;
        private System.Windows.Forms.Label lbPaymentPerMonth;
        private System.Windows.Forms.Label lbTexPerMonth;
        private System.Windows.Forms.Label lbCashRentPerMonth;
        private System.Windows.Forms.TextBox txtDiscountPerMonth;
        private System.Windows.Forms.TextBox txtCashRentDiscountPerMonth;
        private System.Windows.Forms.Label lbCashRentDiscountPerMonth;
        private System.Windows.Forms.Label lbDiscountPerMonth;
        private System.Windows.Forms.GroupBox gbRent;
        private System.Windows.Forms.Label lbPersent2;
        private System.Windows.Forms.Label lbPersent;
        private System.Windows.Forms.Label lbBaht4;
        private System.Windows.Forms.Label lbMonth;
        private System.Windows.Forms.Label lbBaht3;
        private System.Windows.Forms.Label lbBaht2;
        private System.Windows.Forms.Label lbBaht;
        private System.Windows.Forms.GroupBox gbInterestInsurance;
        private System.Windows.Forms.RadioButton rdoAIA;
        private System.Windows.Forms.Label lbAge;
        private System.Windows.Forms.RadioButton rdoNoInsurance;
        private System.Windows.Forms.Label lbInsurance;
        private System.Windows.Forms.Label lbCompanyInsurance;
        private System.Windows.Forms.Label lbBirthday;
        private System.Windows.Forms.TextBox txtCompanyInsurance;
        private System.Windows.Forms.ComboBox cboGender;
        private System.Windows.Forms.RadioButton rdoMTL;
        private System.Windows.Forms.Label lbPriceInsurance;
        private System.Windows.Forms.Label lbRateInterestInsurance;
        private System.Windows.Forms.Label lbSex;
        private System.Windows.Forms.TextBox txtPriceInsurance;
        private System.Windows.Forms.TextBox txtRateInterestInsurance;
        private System.Windows.Forms.Label lbBaht6;
        private System.Windows.Forms.Label lbBaht9;
        private System.Windows.Forms.Label lbBaht10;
        private System.Windows.Forms.Label lbBaht11;
        private System.Windows.Forms.Label lbBaht13;
        private System.Windows.Forms.Label lbBaht12;
        private System.Windows.Forms.Label lbBaht8;
        private System.Windows.Forms.Label lbBaht7;
        private System.Windows.Forms.RadioButton rdoRent;
        private System.Windows.Forms.RadioButton rdoLoan;
        private System.Windows.Forms.Label lbYear;
        private System.Windows.Forms.Label lbBaht5;
        private System.Windows.Forms.DateTimePicker dtpBirthday;
        private System.Windows.Forms.Label lbPersent3;
        private System.Windows.Forms.TextBox txtAge;
    }
}

