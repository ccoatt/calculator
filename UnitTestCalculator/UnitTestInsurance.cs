﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculateService;
using ConnectService;
using CalculationModel;

namespace UnitTestCalculator
{
    /// <summary>
    /// Summary description for UnitTestInsurance
    /// </summary>
    [TestClass]
    public class UnitTestInsurance
    {
        CalculationLoan m_calL = new CalculationLoan();
        CalculationInsurance m_calIns = new CalculationInsurance();
        LoanRentModel m_loanRentModel = new LoanRentModel();
        OutputModel m_outputModel = new OutputModel();
        InsuranceModel m_InsModel = new InsuranceModel();
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private void SetVariable()
        {
            m_loanRentModel.Cash = 100000;
            m_loanRentModel.CashDown = 30000;
            m_loanRentModel.TimePerMonth = 36;
            m_loanRentModel.RateRent = 15;
            m_loanRentModel.RateRentTrue = 12;

            m_InsModel.InsuranceCompany = "AIA";
            m_InsModel.Gender = "ชาย";
            m_InsModel.Birthday = 15;
            m_InsModel.BirthdayMonth = 07;
            m_InsModel.BirthdayYear = 1997; // 2540
            
        }

        [TestMethod]
        public void TestAIALoanAge()
        {
            SetVariable();
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel,m_loanRentModel, m_outputModel);
            double expectAgeAIALoan = m_outputModel.ShowAge;

            Assert.AreEqual(19, expectAgeAIALoan);
        }

        [TestMethod]
        public void TestAIALoanRateInsurance()
        {
            SetVariable();
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            double expectRateInsuranceAIALoan = m_outputModel.ShowRateInsurance;

            Assert.AreEqual(0.37, expectRateInsuranceAIALoan);
        }

        [TestMethod]
        public void TestAIALoanPriceInsurance()
        {
            SetVariable();
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel);
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel, m_outputModel);
            double expectPriceInsuranceLoan = m_outputModel.ShowPriceInsurance;

            Assert.AreEqual(1127, expectPriceInsuranceLoan);
        }

        [TestMethod]
        public void TestAIALoanCashRentTotal()
        {
            SetVariable();
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel);
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel, m_outputModel);
            double expectCashRentTotal = m_outputModel.ShowCashTotal;

            Assert.AreEqual(71127.00, expectCashRentTotal);
        }

        [TestMethod]
        public void TestAIALoanInterestTotal()
        {
            SetVariable();
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel);
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel, m_outputModel);
            double expectInterestTotal = m_outputModel.ShowInterestTotal;

            Assert.AreEqual(32007.15, expectInterestTotal);
        }

        [TestMethod]
        public void TestAIALoanTotalCashWithInterest()
        {
            SetVariable();
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel);
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel, m_outputModel);
            double expectTotalCashWithInterest = m_outputModel.ShowTotalCashWithInterest;

            Assert.AreEqual(103134.15, expectTotalCashWithInterest);
        }

        [TestMethod]
        public void TestAIALoanCashRentPerMonth()
        {
            SetVariable();
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel);
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel, m_outputModel);
            double expectTotalCashRentPerMonth = m_outputModel.ShowCashRentPerMonth;

            Assert.AreEqual(2864.84, expectTotalCashRentPerMonth);
        }

        [TestMethod]
        public void TestMTLLoanAge()
        {
            SetVariable();
            m_InsModel.InsuranceCompany = "MTL";
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            double expectAgMTLLoanAge = m_outputModel.ShowAge;

            Assert.AreEqual(20, expectAgMTLLoanAge);
        }

        [TestMethod]
        public void TestMTLLoanRateInsurance()
        {
            SetVariable();
            m_InsModel.InsuranceCompany = "MTL";
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            double expectRateInsuranceMTLLoan = m_outputModel.ShowRateInsurance;

            Assert.AreEqual(1.08, expectRateInsuranceMTLLoan);
        }

        [TestMethod]
        public void TestMTLLoanPriceInsurance()
        {
            SetVariable();
            m_InsModel.InsuranceCompany = "MTL";
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel);
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            m_outputModel = m_calL.CalSysLoan(m_loanRentModel, m_outputModel);
            double expectPriceInsuranceLoan = m_outputModel.ShowPriceInsurance;

            Assert.AreEqual(1097, expectPriceInsuranceLoan);
        }

        [TestMethod]
        public void TestAIARentPriceInsurance()
        {
            SetVariable();
            m_outputModel = m_calL.CalSysRent(m_loanRentModel);
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            m_outputModel = m_calL.CalSysRent(m_loanRentModel, m_outputModel);
            double expectPriceInsurance = m_outputModel.ShowPriceInsurance;

            Assert.AreEqual(1206, expectPriceInsurance);
        }

        [TestMethod]
        public void TestAIARentCashRentTotal()
        {
            SetVariable();
            m_outputModel = m_calL.CalSysRent(m_loanRentModel);
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            m_outputModel = m_calL.CalSysRent(m_loanRentModel, m_outputModel);
            double expectCashRentTotal = m_outputModel.ShowCashTotal;

            Assert.AreEqual(71206.00, expectCashRentTotal);
        }

        [TestMethod]
        public void TestAIARentInterestTotal()
        {
            SetVariable();
            m_outputModel = m_calL.CalSysRent(m_loanRentModel);
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            m_outputModel = m_calL.CalSysRent(m_loanRentModel, m_outputModel);
            double expectInterestTotal = m_outputModel.ShowInterestTotal;

            Assert.AreEqual(32042.70, expectInterestTotal);
        }


        [TestMethod]
        public void TestAIARentTotalCashWithInterest()
        {
            SetVariable();
            m_outputModel = m_calL.CalSysRent(m_loanRentModel);
            m_outputModel = m_calIns.CalSysInsurance(m_InsModel, m_loanRentModel, m_outputModel);
            m_outputModel = m_calL.CalSysRent(m_loanRentModel, m_outputModel);
            double expectTotalCashWithInterest = m_outputModel.ShowTotalCashWithInterest;

            Assert.AreEqual(110476.11, expectTotalCashWithInterest);
        }

    }
}
