﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculateService;


namespace UnitTestCalculator
{
    [TestClass]
    public class UnitTestCalculator
    {
        public double m_expectCalCashResult = 0;
        public double m_expectCalInterestTotal = 0;
        public double m_expectCalTotalCashWithInterest = 0;
        public double m_expectCalCashRentPerMonth = 0;
        public double m_expectCalCashRentDiscountPerMonth = 0;
        public double m_expectCalDiscountPerMonth = 0;

        public double m_expectCalVat = 0;
        public double m_expectCalVatPerMonth = 0;
        public double m_expectCalRentCashRentPerMonth = 0;

        CalculationLoan m_calL = new CalculationLoan();
        //public double m_expectCalCashRentDiscountPerMonth;
        [TestMethod]
        public void TestCalCashRasult()
        {
            m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
            Assert.AreEqual(70000, m_expectCalCashResult);

        }

        [TestMethod]
        public void TestCalInterestTotal()
        {
            m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
            m_expectCalInterestTotal = m_calL.CalInterestTotal(m_expectCalCashResult, 15, 24);
            Assert.AreEqual(21000, m_expectCalInterestTotal);
        }

        [TestMethod]
        public void TestCalTotalCashWithInterest()
        {
            m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
            m_expectCalInterestTotal = m_calL.CalInterestTotal(m_expectCalCashResult, 15, 24);
            m_expectCalTotalCashWithInterest = m_calL.CalTotalCashWithInterest(m_expectCalCashResult, m_expectCalInterestTotal);
            Assert.AreEqual(91000, m_expectCalTotalCashWithInterest);
        }

        [TestMethod]
        public void TestCalCashRentPerMonth()
        {
            m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
            m_expectCalInterestTotal = m_calL.CalInterestTotal(m_expectCalCashResult, 15, 24);
            m_expectCalTotalCashWithInterest = m_calL.CalTotalCashWithInterest(m_expectCalCashResult, m_expectCalInterestTotal);
            m_expectCalCashRentPerMonth = m_calL.CalCashRentPerMonth(m_expectCalTotalCashWithInterest, 24);
            Assert.AreEqual(3791.67, m_expectCalCashRentPerMonth);
        }

        [TestMethod]
        public void TestCalCashRentDiscountPerMonth()
        {
            m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
            m_expectCalCashRentDiscountPerMonth = m_calL.CalCashRentDiscountPerMonth(m_expectCalCashResult, 12, 24);
            Assert.AreEqual(3616.67, m_expectCalCashRentDiscountPerMonth);
        }

        [TestMethod]
        public void TestCalDiscountPerMonth()
        {
            m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
            m_expectCalCashRentDiscountPerMonth = m_calL.CalCashRentDiscountPerMonth(m_expectCalCashResult, 12, 24);
            
            m_expectCalInterestTotal = m_calL.CalInterestTotal(m_expectCalCashResult, 15, 24);
            m_expectCalTotalCashWithInterest = m_calL.CalTotalCashWithInterest(m_expectCalCashResult, m_expectCalInterestTotal);
            m_expectCalCashRentPerMonth = m_calL.CalCashRentPerMonth(m_expectCalTotalCashWithInterest, 24);
            m_expectCalDiscountPerMonth = m_calL.CalDiscountPerMonth(m_expectCalCashRentDiscountPerMonth, m_expectCalCashRentPerMonth);
            Assert.AreEqual(175, m_expectCalDiscountPerMonth);
        }

        


        [TestMethod]
        public void TestCalRentVat()
        {
            m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
            m_expectCalInterestTotal = m_calL.CalInterestTotal(m_expectCalCashResult, 15, 24);

            m_expectCalVat = m_calL.CalVat7(m_expectCalCashResult,m_expectCalInterestTotal);
            Assert.AreEqual(6370.00, m_expectCalVat);
        }

        [TestMethod]
        public void TestCalRentTotalCashWithInterest()
        {
            m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
            m_expectCalInterestTotal = m_calL.CalInterestTotal(m_expectCalCashResult, 15, 24);
            m_expectCalVat = m_calL.CalVat7(m_expectCalCashResult, m_expectCalInterestTotal);

            m_expectCalTotalCashWithInterest = m_calL.CalTotalCashWithInterest(m_expectCalCashResult, m_expectCalInterestTotal, m_expectCalVat);
            Assert.AreEqual(97370.00, m_expectCalTotalCashWithInterest);
        }



        [TestMethod]
        public void TestCalRentPaymentPerMonth()
        {
            m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
            m_expectCalInterestTotal = m_calL.CalInterestTotal(m_expectCalCashResult, 15, 24);

            double expectCalPaymentPerMonth = m_calL.CalPaymentPerMonth(m_expectCalCashResult, m_expectCalInterestTotal, 24);
            Assert.AreEqual(3791.67, expectCalPaymentPerMonth);
        }



        [TestMethod]
        public void TestCalRentVatPerMonth()
        {
            m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
            m_expectCalInterestTotal = m_calL.CalInterestTotal(m_expectCalCashResult, 15, 24);
            m_expectCalVat = m_calL.CalVat7(m_expectCalCashResult, m_expectCalInterestTotal);

            m_expectCalVatPerMonth = m_calL.CalVatPerMonth(m_expectCalVat, 24);
            Assert.AreEqual(265.42, m_expectCalVatPerMonth);
        }
        

     [TestMethod]
     public void TestCalRentCashRentPerMonth()
     {
         m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
         m_expectCalInterestTotal = m_calL.CalInterestTotal(m_expectCalCashResult, 15, 24);
         m_expectCalVat = m_calL.CalVat7(m_expectCalCashResult, m_expectCalInterestTotal);
         m_expectCalTotalCashWithInterest = m_calL.CalTotalCashWithInterest(m_expectCalCashResult, m_expectCalInterestTotal, m_expectCalVat);

         m_expectCalRentCashRentPerMonth = m_calL.CalCashRentPerMonth(m_expectCalTotalCashWithInterest, 24);
         Assert.AreEqual(4057.08, m_expectCalRentCashRentPerMonth);
     }

    [TestMethod]
     public void TestCalRentCashDiscoutPerMonth()
     {
         m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 

         m_expectCalCashRentDiscountPerMonth = m_calL.CalCashRentDiscountPerMonthVat(m_expectCalCashResult, 12, 24);
         Assert.AreEqual(3869.83, m_expectCalCashRentDiscountPerMonth);
     }

    [TestMethod]
    public void TestCalRentDiscoutPerMonth()
    {

        m_expectCalCashResult = m_calL.CalCashRasult(100000, 30000); 
        m_expectCalInterestTotal = m_calL.CalInterestTotal(m_expectCalCashResult, 15, 24);
        m_expectCalVat = m_calL.CalVat7(m_expectCalCashResult, m_expectCalInterestTotal);
        m_expectCalTotalCashWithInterest = m_calL.CalTotalCashWithInterest(m_expectCalCashResult, m_expectCalInterestTotal, m_expectCalVat);

        m_expectCalRentCashRentPerMonth = m_calL.CalCashRentPerMonth(m_expectCalTotalCashWithInterest, 24);
        m_expectCalCashRentDiscountPerMonth = m_calL.CalCashRentDiscountPerMonthVat(m_expectCalCashResult, 12, 24);

        double expectCalDiscountPerMonth = m_calL.CalDiscountPerMonth(m_expectCalCashRentDiscountPerMonth, m_expectCalRentCashRentPerMonth);
        Assert.AreEqual(187.25, expectCalDiscountPerMonth);
    }

    
 
       
    }
}
