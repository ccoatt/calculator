﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculationModel;

namespace CalculateService
{
    public  class CalculationLoan 
    {
        // คำนวน ราคาส่วนที่เหลือ
        public  double CalCashRasult(Int64 cash, double cashDown)
        {
            return Math.Round(cash - cashDown, 2, MidpointRounding.AwayFromZero);
        }

        // ค่าเช่าซื้อทั้งหมด
        public double CalCashRentTotal(double calCashResult, double priceInsurance)
        {
            return Math.Round(calCashResult + priceInsurance, 2, MidpointRounding.AwayFromZero);
        }

        // คำนวนดอกเบี้ยเช่าทั้งหมด
        public double CalInterestTotal(double calCashRentTotal, double rate, double time)
        {
            //double rateDouble = rate / 100;
            return Math.Round((calCashRentTotal * rate * (time / 12)) / 100, 2, MidpointRounding.AwayFromZero);
        }

        // ค่าเชื่อซื้อรวมดอกเบี้ย(ทุนประกัน)
        public double CalTotalCashWithInterest(double calCashRentTotal, double calInterestTotal)
        {
            return Math.Round(calCashRentTotal + calInterestTotal, 2, MidpointRounding.AwayFromZero);
        }

        // ค่าเชื่อซื้อรวมดอกเบี้ย(ทุนประกัน) + vat7 
        public double CalTotalCashWithInterest(double calCashRentTotal, double calInterestTotal, double calVat)
        {
            return Math.Round(calCashRentTotal + calInterestTotal + calVat, 2, MidpointRounding.AwayFromZero);
        }

        // ค่าเช่าซื้อ/เดือน
        public  double CalCashRentPerMonth(double calTotalCashWithInterest, double time)
        {
            return Math.Round(calTotalCashWithInterest / time, 2, MidpointRounding.AwayFromZero);
        }

        // ค่าเช่าซื้อส่วนลด/เดือน
        public double CalCashRentDiscountPerMonth(double calCashRentTotal, double rate, double time)
        {
            double calInterest = (calCashRentTotal * rate * (time / 12)) / 100;
            return Math.Round((calCashRentTotal + calInterest) / time, 2, MidpointRounding.AwayFromZero);
        }

        // ค่าเช่าซื้อส่วนลด/เดือน + vat7
        public double CalCashRentDiscountPerMonthVat(double calCashRentTotal, double rate, double time)
        {
            double calInterest = (calCashRentTotal * rate * (time / 12)) / 100;
            double calVat = ((calCashRentTotal + calInterest) * 7) / 100;
            return Math.Round((calCashRentTotal + calInterest + calVat) / time, 2, MidpointRounding.AwayFromZero);
        }

        // ส่วนลด/เดือน
        public  double CalDiscountPerMonth(double calCashRentDiscountPerMonth, double calCashRentPerMonth)
        {
            return Math.Round(calCashRentPerMonth - calCashRentDiscountPerMonth, 2, MidpointRounding.AwayFromZero);
        }

        // ค่างวดเดือน
        public double CalPaymentPerMonth(double calCashRentTotal, double calInterestTotal, double time)
        {
            //return Math.Floor(((calCashRentTotal + calInterestTotal) / time)*100)/100;
            return Math.Ceiling(((calCashRentTotal + calInterestTotal) / time) * 100) / 100;
            //return Math.Round((calCashRentTotal + calInterestTotal) / time ,2) ;
        }

        // VAT 7 %
        public double CalVat7(double calCashRentTotal, double calInterestTotal)
        {
            return Math.Round(((calCashRentTotal + calInterestTotal) * 7) / 100, 2, MidpointRounding.AwayFromZero);
        }

        // VAT ต่อเดือน
        public double CalVatPerMonth(double calVat, double time)
        {
            return Math.Round(calVat / time, 2, MidpointRounding.AwayFromZero);
        }

        ////
        public OutputModel CalSysLoan(LoanRentModel lrModel)
        {
            OutputModel outputModel = new OutputModel();
              // คำนวน ราคาส่วนที่เหลือ 
            outputModel.ShowCashResult = CalCashRasult(lrModel.Cash, lrModel.CashDown);

            //
            outputModel.ShowCashTotal = CalCashRentTotal(outputModel.ShowCashResult, outputModel.ShowPriceInsurance);


            // คำนวนดอกเบี้ยเช่าทั้งหมด
            outputModel.ShowInterestTotal = CalInterestTotal(outputModel.ShowCashTotal, lrModel.RateRent, lrModel.TimePerMonth);

            // ค่าเชื่อซื้อรวมดอกเบี้ย(ทุนประกัน)
            outputModel.ShowTotalCashWithInterest = CalTotalCashWithInterest(outputModel.ShowCashTotal, outputModel.ShowInterestTotal);

            // ค่าเช่าซื้อ/เดือน
            outputModel.ShowCashRentPerMonth = CalCashRentPerMonth(outputModel.ShowTotalCashWithInterest, lrModel.TimePerMonth);

            // ค่าเช่าซื้อส่วนลด/เดือน
            outputModel.ShowCashRentDiscountPerMonth = CalCashRentDiscountPerMonth(outputModel.ShowCashTotal, lrModel.RateRentTrue, lrModel.TimePerMonth);

            // ส่วนลด/เดือน
            outputModel.ShowDiscountPerMonth = CalDiscountPerMonth(outputModel.ShowCashRentDiscountPerMonth, outputModel.ShowCashRentPerMonth);

            return outputModel;
        }

        //  + ประกัน
        public OutputModel CalSysLoan(LoanRentModel lrModel, OutputModel outputModel)
        {
            //OutputModel outputModel = new OutputModel();
            // คำนวน ราคาส่วนที่เหลือ 
            outputModel.ShowCashResult = CalCashRasult(lrModel.Cash, lrModel.CashDown);

            //
            outputModel.ShowCashTotal = CalCashRentTotal(outputModel.ShowCashResult, outputModel.ShowPriceInsurance);

            // คำนวนดอกเบี้ยเช่าทั้งหมด
            outputModel.ShowInterestTotal = CalInterestTotal(outputModel.ShowCashTotal, lrModel.RateRent, lrModel.TimePerMonth);

            // ค่าเชื่อซื้อรวมดอกเบี้ย(ทุนประกัน)
            outputModel.ShowTotalCashWithInterest = CalTotalCashWithInterest(outputModel.ShowCashTotal, outputModel.ShowInterestTotal);

            // ค่าเช่าซื้อ/เดือน
            outputModel.ShowCashRentPerMonth = CalCashRentPerMonth(outputModel.ShowTotalCashWithInterest, lrModel.TimePerMonth);

            // ค่าเช่าซื้อส่วนลด/เดือน
            outputModel.ShowCashRentDiscountPerMonth = CalCashRentDiscountPerMonth(outputModel.ShowCashTotal, lrModel.RateRentTrue, lrModel.TimePerMonth);

            // ส่วนลด/เดือน
            outputModel.ShowDiscountPerMonth = CalDiscountPerMonth(outputModel.ShowCashRentDiscountPerMonth, outputModel.ShowCashRentPerMonth);

            return outputModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lrModel"></param>
        /// <param name="outputModel"></param>
        public OutputModel CalSysRent(LoanRentModel lrModel)
        {
            OutputModel outputModel = new OutputModel();
            // คำนวน ราคาส่วนที่เหลือ 
            outputModel.ShowCashResult = CalCashRasult(lrModel.Cash, lrModel.CashDown);

            //
            outputModel.ShowCashTotal = CalCashRentTotal(outputModel.ShowCashResult, outputModel.ShowPriceInsurance);

            // คำนวนดอกเบี้ยเช่าทั้งหมด
            outputModel.ShowInterestTotal = CalInterestTotal(outputModel.ShowCashTotal, lrModel.RateRent, lrModel.TimePerMonth);

            // คำนวน vat
            outputModel.ShowVat = CalVat7(outputModel.ShowCashTotal, outputModel.ShowInterestTotal);

            // ค่าเชื่อซื้อรวมดอกเบี้ย(ทุนประกัน) + vat
            outputModel.ShowTotalCashWithInterest = CalTotalCashWithInterest(outputModel.ShowCashTotal, outputModel.ShowInterestTotal, outputModel.ShowVat);

            // ค่างวด/เดือน
            outputModel.ShowPaymentPerMonth = CalPaymentPerMonth(outputModel.ShowCashTotal, outputModel.ShowInterestTotal, lrModel.TimePerMonth);

            // ค่าภาษี / เดือน
            outputModel.ShowVatPerMonth = CalVatPerMonth(outputModel.ShowVat, lrModel.TimePerMonth);

            // ค่าเช่าซื้อ/เดือน
            outputModel.ShowCashRentPerMonth = CalCashRentPerMonth(outputModel.ShowTotalCashWithInterest, lrModel.TimePerMonth);

            // ค่าเช่าซื้อส่วนลด/เดือน
            outputModel.ShowCashRentDiscountPerMonth = CalCashRentDiscountPerMonthVat(outputModel.ShowCashTotal, lrModel.RateRentTrue, lrModel.TimePerMonth);

            // ส่วนลด/เดือน
            outputModel.ShowDiscountPerMonth = CalDiscountPerMonth(outputModel.ShowCashRentDiscountPerMonth, outputModel.ShowCashRentPerMonth);

            return outputModel;
        }

        /// <summary>
        ///  + ประกัน
        /// </summary>
        /// <param name="lrModel"></param>
        /// <returns></returns>
        public OutputModel CalSysRent(LoanRentModel lrModel, OutputModel outputModel)
        {
            //OutputModel outputModel = new OutputModel();
            // คำนวน ราคาส่วนที่เหลือ 
            outputModel.ShowCashResult = CalCashRasult(lrModel.Cash, lrModel.CashDown);

            //
            outputModel.ShowCashTotal = CalCashRentTotal(outputModel.ShowCashResult, outputModel.ShowPriceInsurance);

            // คำนวนดอกเบี้ยเช่าทั้งหมด
            outputModel.ShowInterestTotal = CalInterestTotal(outputModel.ShowCashTotal, lrModel.RateRent, lrModel.TimePerMonth);

            // คำนวน vat
            outputModel.ShowVat = CalVat7(outputModel.ShowCashTotal, outputModel.ShowInterestTotal);

            // ค่าเชื่อซื้อรวมดอกเบี้ย(ทุนประกัน) + vat
            outputModel.ShowTotalCashWithInterest = CalTotalCashWithInterest(outputModel.ShowCashTotal, outputModel.ShowInterestTotal, outputModel.ShowVat);

            // ค่างวด/เดือน
            outputModel.ShowPaymentPerMonth = CalPaymentPerMonth(outputModel.ShowCashTotal, outputModel.ShowInterestTotal, lrModel.TimePerMonth);

            // ค่าภาษี / เดือน
            outputModel.ShowVatPerMonth = CalVatPerMonth(outputModel.ShowVat, lrModel.TimePerMonth);

            // ค่าเช่าซื้อ/เดือน
            outputModel.ShowCashRentPerMonth = CalCashRentPerMonth(outputModel.ShowTotalCashWithInterest, lrModel.TimePerMonth);

            // ค่าเช่าซื้อส่วนลด/เดือน
            outputModel.ShowCashRentDiscountPerMonth = CalCashRentDiscountPerMonthVat(outputModel.ShowCashTotal, lrModel.RateRentTrue, lrModel.TimePerMonth);

            // ส่วนลด/เดือน
            outputModel.ShowDiscountPerMonth = CalDiscountPerMonth(outputModel.ShowCashRentDiscountPerMonth, outputModel.ShowCashRentPerMonth);

            return outputModel;
        }
    }
}
