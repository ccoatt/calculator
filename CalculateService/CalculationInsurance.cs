﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculationModel;
using ConnectService;

namespace CalculateService
{
    public class CalculationInsurance 
    {
        public int ShowYear {get;set;}
        public int ShowMonth { get; set; }
        public int ShowDay { get; set; }

        // คำนวนอายุ
        public int CalAge(int birthDayYear)
        {
            return DateTime.Today.Year - birthDayYear;
        }

        /// คำนวนอายุบริบูนณ์
        public void CalAgeComplete(int birthDay, int birthDayMonth, int birthDayYear)
        {
            int nowDay = DateTime.Today.Day;
            int nowMonth = DateTime.Today.Month;
            int nowYear = DateTime.Today.Year;

            if ((nowYear - birthDayYear) > 0 ||
        (((nowYear - birthDayYear) == 0) && ((birthDayMonth < nowMonth) ||
          ((birthDayMonth == nowMonth) && (birthDay <= nowDay)))))
            {
                int DaysInBdayMonth = DateTime.DaysInMonth(birthDayYear, birthDayMonth);
                int DaysRemain = nowDay + (DaysInBdayMonth - birthDay);

                if (nowMonth > birthDayMonth)
                {
                    ShowYear = nowYear - birthDayYear;
                    ShowMonth = nowMonth - (birthDayMonth + 1) + Math.Abs(DaysRemain / DaysInBdayMonth);
                    ShowDay = (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
                }
                else if (nowMonth == birthDayMonth)
                {
                    if (nowDay >= birthDay)
                    {
                        ShowYear = nowYear - birthDayYear;
                        ShowMonth = 0;
                        ShowDay = nowDay - birthDay;
                    }
                    else
                    {
                        ShowYear = (nowYear - 1) - birthDayYear;
                        ShowMonth = 11;
                        ShowDay = DateTime.DaysInMonth(birthDayYear, birthDayMonth) - (birthDay - nowDay);
                    }
                }
                else
                {
                    ShowYear = (nowYear - 1) - birthDayYear;
                    ShowMonth = nowMonth + (11 - birthDayMonth) + Math.Abs(DaysRemain / DaysInBdayMonth);
                    ShowDay = (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
                }
            }
            //return this;
        }

        /// คำนวนค่าเบี้ยประกัน AIA
        public double CalPriceInsurance(double totalCashWithInterest,double rateInsurance,double time)
        {
            return Math.Ceiling(totalCashWithInterest*((rateInsurance/100)*(time/12)));
        }

        /// คำนวนค่าเบี้ยประกัน MTL
        public double CalPriceInsuranceMTL(double totalCashWithInterest, double rateInsurance, double time)
        {
            return Math.Ceiling(totalCashWithInterest * (rateInsurance / 100));
        }

        ////
        public OutputModel CalAgeRate(InsuranceModel insModel,LoanRentModel loanRentModel, OutputModel outputModel)
        {
            ConnectDB conDB = new ConnectDB();

            switch (insModel.InsuranceCompany)
            {
                case "AIA": CalAgeComplete(insModel.Birthday, insModel.BirthdayMonth, insModel.BirthdayYear);
                    outputModel.ShowAge = this.ShowYear;
                    outputModel.ShowRateInsurance = conDB.Connect(insModel.InsuranceCompany, insModel.Gender, outputModel.ShowAge, loanRentModel.TimePerMonth);
                    break;
                case "MTL": outputModel.ShowAge = CalAge(insModel.BirthdayYear);
                    outputModel.ShowRateInsurance = conDB.Connect(insModel.InsuranceCompany, insModel.Gender, outputModel.ShowAge, loanRentModel.TimePerMonth);
                    break;
            }
            return outputModel;
        }


        ////
        public OutputModel CalSysInsurance(InsuranceModel insModel,LoanRentModel loanRentModel,  OutputModel outputModel)
        {
            ConnectDB conDB = new ConnectDB();

            switch (insModel.InsuranceCompany)
            {
                case "AIA": CalAgeComplete(insModel.Birthday, insModel.BirthdayMonth, insModel.BirthdayYear);
                    outputModel.ShowAge = this.ShowYear;
                    outputModel.ShowRateInsurance = conDB.Connect(insModel.InsuranceCompany, insModel.Gender, outputModel.ShowAge, loanRentModel.TimePerMonth);
                    outputModel.ShowPriceInsurance = CalPriceInsurance(outputModel.ShowTotalCashWithInterest, outputModel.ShowRateInsurance, loanRentModel.TimePerMonth);
                    break;
                case "MTL": outputModel.ShowAge = CalAge(insModel.BirthdayYear);
                    outputModel.ShowRateInsurance = conDB.Connect(insModel.InsuranceCompany, insModel.Gender, outputModel.ShowAge, loanRentModel.TimePerMonth);
                    outputModel.ShowPriceInsurance = CalPriceInsuranceMTL(outputModel.ShowTotalCashWithInterest, outputModel.ShowRateInsurance, loanRentModel.TimePerMonth);
                    break;
            }

            return outputModel;
        }


    }
}
