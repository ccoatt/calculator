﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateService
{
    public class Calculate
    {
        // คำนวน ราคาส่วนที่เหลือ
        public double  CalCashRasult(double cash, double cashDown)
        {
            return Math.Round(cash - cashDown,2);
        }

        // คำนวนดอกเบี้ยเช่าทั้งหมด
        public double CalInterestTotal(double calCashResult, double rate, double time)
        {
            //double rateDouble = rate / 100;
            return Math.Round( (calCashResult *rate * (time / 12))/100 ,2);
        }

        // ค่าเชื่อซื้อรวมดอกเบี้ย(ทุนประกัน)
        public double CalTotalCashWithInterest(double calCashResult, double calInterestTotal)
        {
            return Math.Round(calCashResult + calInterestTotal ,2);
        }

        // ค่าเชื่อซื้อรวมดอกเบี้ย(ทุนประกัน) + vat7 
        public double CalTotalCashWithInterest(double calCashResult, double calInterestTotal , double calVat)
        {
            return Math.Round(calCashResult + calInterestTotal + calVat, 2);
        }

        // ค่าเช่าซื้อ/เดือน
        public double CalCashRentPerMonth(double calTotalCashWithInterest, double time)
        {
            return Math.Round(calTotalCashWithInterest / time,2);
        }

        // ค่าเช่าซื้อส่วนลด/เดือน
        public double CalCashRentDiscountPerMonth(double calCashResult, double rate, double time) 
        {
            double calInterest = (calCashResult * rate * (time / 12)) / 100;
            return Math.Round( (calCashResult + calInterest) / time ,2);
        }

        // ค่าเช่าซื้อส่วนลด/เดือน + vat7
        public double CalCashRentDiscountPerMonthVat(double calCashResult, double rate, double time)
        {
            double calInterest = (calCashResult * rate * (time / 12)) / 100;
            double calVat = ((calCashResult + calInterest)*7)/100 ;
            return Math.Round((calCashResult + calInterest + calVat ) / time, 2);
        }

        // ส่วนลด/เดือน
        public double CalDiscountPerMonth(double calCashRentDiscountPerMonth, double calCashRentPerMonth)
        {
            return Math.Round(calCashRentPerMonth - calCashRentDiscountPerMonth,3);
        }

        // VAT 7 %
        public double CalVat7(double calCashResult, double calInterestTotal)
        {
            return Math.Round( ((calCashResult + calInterestTotal)*7)/100 , 2);
        }

        // VAT ต่อเดือน
        public double CalVatPerMonth(double calVat, double time)
        {
            return Math.Round(calVat/time,2);
        }

        // ค่างวดเดือน
        public double CalPaymentPerMonth(double calCashResult, double calInterestTotal, double time)
        {
            return Math.Round((calCashResult + calInterestTotal)/time, 2);
        }

        // คำนวนอายุ
        public int CalAge(int birthday)
        {
            return DateTime.Today.Year - birthday; 
        }
    }
}
